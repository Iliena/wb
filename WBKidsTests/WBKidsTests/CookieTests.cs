﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.Selenium;
using Fuel.QA.Selenium.Extensions;
using Fuel.QA.WBKidsTests.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;


namespace Fuel.QA.WBKidsTests
{
    [TestFixture]
    class CookieTests
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            var drv = new FirefoxDriver();
            //var drv = new FirefoxDriver();
            driver = new LoggableWebDriver(drv);
            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            driver.Stop();
        }

        public IEnumerable MakeTestSource(string testPage)
        {
            var sites = System.Environment.GetEnvironmentVariable("WBKIDSENV");
            if (sites == null)
                sites = ConfigurationManager.AppSettings["WBKIDSENV"];

            var regions = System.Environment.GetEnvironmentVariable("WBKIDSREGIONS");
            if (regions == null)
                regions = ConfigurationManager.AppSettings["WBKIDSREGIONS"];

            foreach (var site in sites.Split(','))
            {
                foreach (var region in regions.Split(','))
                {
                    var lng = region.Split('-')[0];
                    var name = site.Replace('.', '-') + "." + region + "." + testPage;
                    yield return new TestCaseData(site, region, lng).SetName(name);
                }
            }
        }
        public IEnumerable ImpliedCookieDialogTestSource()
        {
            return MakeTestSource("Implied cookie dialog");
        }

        [Test, TestCaseSource("ImpliedCookieDialogTestSource")]
        public void TestImpliedCookieDialog(string host, string locale, string lng)
        {
            var homePage = new HomePage(this.driver, host, locale).Load();
            Assert.That(ImpliedCookieDialog.IsDialogPresent(this.driver), Is.EqualTo(CookieHandling.NeedToAppear(CookieType.Implied, locale)),
                 "The cookie implied dialog appearance differ from expected");
        }
        public IEnumerable CookieExpressDialogTestSource()
        {
            return MakeTestSource("Express cookie dialog");
        }

        [Test, TestCaseSource("CookieExpressDialogTestSource")]
        public void TestExpressCookieDialog(string host, string locale, string lng)
        {
            var homePage = new HomePage(this.driver, host, locale).Load();
            Assert.That(ExpressCookieDialog.IsDialogPresent(this.driver), Is.EqualTo(CookieHandling.NeedToAppear(CookieType.Express, locale)),
                 "The cookie express dialog appearance differ from expected");
        }

    }

}
