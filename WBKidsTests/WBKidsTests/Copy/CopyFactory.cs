﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.Copy
{
    public static class CopyFactory
    {
        public static List<IWebElement> GetElements(object page)
        {
            if (page == null)
            {
                throw new ArgumentNullException("page", "page cannot be null");
            }

            // Get a list of all of the fields and properties (public and non-public [private, protected, etc.])
            // in the passed-in page object. Note that we walk the inheritance tree to get superclass members.
            var type = page.GetType();
            var members = new List<MemberInfo>();
            const BindingFlags PublicBindingOptions = BindingFlags.Instance | BindingFlags.Public;
            members.AddRange(type.GetFields(PublicBindingOptions));
            members.AddRange(type.GetProperties(PublicBindingOptions));
            while (type != null)
            {
                const BindingFlags NonPublicBindingOptions = BindingFlags.Instance | BindingFlags.NonPublic;
                members.AddRange(type.GetFields(NonPublicBindingOptions));
                members.AddRange(type.GetProperties(NonPublicBindingOptions));
                type = type.BaseType;
            }

            List<IWebElement> elements = new List<IWebElement>();
            // Examine each member, and if it is both marked with an appropriate attribute, and of
            // the proper type, set the member's value to the appropriate type of proxy object.
            foreach (var member in members)
            {
                var attributes = Attribute.GetCustomAttributes(member, typeof(CopyAttribute), true);
                if (attributes.Length > 0)
                {
                    Array.Sort(attributes);
                    foreach (var attribute in attributes)
                    {
                        var castedAttribute = (CopyAttribute)attribute;
                    }

                    object obj = null;
                    var field = member as FieldInfo;
                    var property = member as PropertyInfo;

                    try
                    {
                        if (field != null)
                            obj = field.GetValue(page);
                        else if (property != null)
                            obj = property.GetValue(page, null);
                    }
                    catch (Exception e)
                    {
                        if (e.GetBaseException() is NoSuchElementException)
                            Console.WriteLine("### NoSuchElementException");
                        else throw;
                    }

                    IWebElement element = obj as IWebElement;
                    IList<IWebElement> elems = obj as IList<IWebElement>;
                    if (element != null)
                    {
                        //element.TryHighlight();
                        elements.Add(element);
                    }
                    else if (elems != null)
                    {
                        elements.AddRange(elems);
                        //elements.ForEach(e => e.TryHighlight());
                    }
                }
            }
            return elements.Where(e => e.IsAttached()).ToList();
        }


        public static List<string> GetCopy(object page)
        {
            var elements = GetElements(page);
            var list = new List<string>();

            foreach (var e in elements)
            {
                list.Add(e.Text);
            }
            return list;
        }
    }
}
