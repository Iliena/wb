﻿using System;

namespace Fuel.QA.WBKidsTests.Copy
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
    public sealed class CopyAttribute : Attribute
    {
        public string Id { get; set; }
        public string FromAttribute { get; set; }

        public CopyAttribute() { }

        public CopyAttribute(string id)
        {
            this.Id = id;
        }
    }
}
