﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;



namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Terms : LoadableComponent<Terms>
    {
        IWebDriver driver;
        Uri uri;

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//h3[@class='title']")]
        [Copy]
        IWebElement Title { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//p | //div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//li")]
        [Copy]
        IList<IWebElement> TermsElements { get; set; }

        By MainElement = By.XPath("//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//h3[@class='title']");


        public Terms(IWebDriver driver, string url)
        {
            this.driver = driver;
            this.uri = new UriBuilder(url).Uri;
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.Url.EndsWith("terms") && driver.FindElements(MainElement).Any();
        }

        protected override void ExecuteLoad() 
        {
            driver.Navigate().GoToUrl(uri.AbsoluteUri);
            PageFactory.InitElements(driver, this);
        }


    }
}
