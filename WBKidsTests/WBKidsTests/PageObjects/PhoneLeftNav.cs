﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class PhoneLeftNav : LoadableComponent<PhoneLeftNav>
    {
        //*[@id="wbk-phone-left-nav"]/div[2]/ul[1]/li[1]
        //*[@id="wbk-phone-left-nav"]/div[2]/ul[1]/li[2]
        //*[@id="wbk-phone-left-nav"]/div[2]/ul[1]/li[3]
        //*[@id="wbk-phone-left-nav"]/div[2]/ul[1]/li[4]

        [FindsBy(How = How.XPath, Using = "//*[@id='wbk-phone-left-nav']/div[2]/ul[1]/li")]
        [Copy]
        IList<IWebElement> HomeNavItems { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='wbk-phone-left-nav']//ul[contains(@class, 'legal-nav')]/li/div/span")]
        [Copy]
        IWebElement LegalNav { get; set; }

        [FindsBy(How = How.XPath, Using="//*[@id='wbk-phone-left-nav']//ul[contains(@class, 'legal-nav')]/li/ul/li")]
        [Copy]
        IList<IWebElement> LegalNavItems { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//*[@id='wbk-phone-top-nav-inner']//span[@class='close-nav']")]
        IWebElement CloseButton { get; set; }

        By MainElementLocator = By.Id("wbk-phone-left-nav");

        private IWebDriver driver;
        System.Drawing.Size size;
        public PhoneLeftNav(IWebDriver driver, System.Drawing.Size size)
        {
            this.driver = driver;
            this.size = size;
            PageFactory.InitElements(driver, this);
        }

        protected override void ExecuteLoad()
        {
            // nothing
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.FindElements(MainElementLocator).Any();
        }

        public void Close()
        {
            CloseButton.Click();
            //driver.Manage().Window.Size = this.size;
            driver.Manage().Window.Maximize();
        }

        public void ClickLegalNav()
        {
            LegalNav.Click();
        }
    }
}
