﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;
using System.Text.RegularExpressions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class HomePage : SlowLoadableComponent<HomePage>
    {
        //[CacheLookup]
        [FindsBy(How = How.XPath, Using = "//div[@id='isotopes']//div[contains(@id,'dnn_ctr') and contains(@id,'_ModuleContent')]/ul[contains(@class, 'isotope')]/li[contains(@class,' default ')]")]
        IList<IWebElement> DetailIsotopeItems { get; set; }

        //[CacheLookup]
        [FindsBy(How = How.XPath, Using = "//div[@id='isotopes']//ul[contains(@class, 'isotope')]/li[contains(@class,' gallery ')]")]
        IList<IWebElement> GalleryIsotopeItems { get; set; }

        private IWebDriver driver;

        string locale;
        Uri host;

        public HomePage(IWebDriver driver, string host, string locale)
            : base(TimeSpan.FromSeconds(10))
        {
            this.driver = driver;
            this.host = new UriBuilder(host).Uri;
            this.locale = locale;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return DetailIsotopeItems.Any() && DetailIsotopeItems.First().IsDisplayed();
        }

        protected override void ExecuteLoad()
        {
            UriBuilder builder = new UriBuilder(host);
            builder.Path = locale;
            driver.Navigate().GoToUrl(builder.Uri);
        }

        public IEnumerable<IsotopeElement> GetIsotopes()
        {
            return GetDetailIsotopes().Concat(GetGalleryIsotopes());
        }

        public IEnumerable<IsotopeElement> GetDetailIsotopes()
        {
            return DetailIsotopeItems.Select(e => new IsotopeElement(e));
        }

        public List<string> GetDetailLinks()
        {
            return GetDetailIsotopes()
                .Where(e => HrefFilter(e.Href))
                .Select(e => e.Href).ToList();
        }

        public IEnumerable<IsotopeElement> GetGalleryIsotopes()
        {
            return GalleryIsotopeItems.Select(e => new IsotopeElement(e));
        }

        public List<string> GetGalleryLinks()
        {
            return GetGalleryIsotopes()
                .Where(e => HrefFilter(e.Href))
                .Select(i => i.Href).ToList();
        }

        private bool HrefFilter(string href)
        {
            if (string.IsNullOrEmpty(href))
                return false;

            var uri = new UriBuilder(href);
            var h = Regex.Replace(uri.Host, "^www.", "");
            return h == Regex.Replace(this.host.Host, "^www.", "");
        }
    }
}
