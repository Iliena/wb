﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Shows : SlowLoadableComponent<Shows>
    {
        private IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNContainer_noTitle']//h2")]
        [Copy]
        IWebElement ShowsTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='wbk-modal-outer']//div[@class='wbk-modal-close']")]
        IWebElement CloseButton { get; set; }

        public string[] ShowUrls
        {
            get
            {
                return driver.FindElements(ModalElements).Select(item => item.GetAttribute("href")).ToArray();
            }
        }
        static By Loader = By.Id("wbk-busy");
        static By ModalElements = By.XPath("//div[@class='wbk-modal-outer']//ul//li/a");

        public Shows(IWebDriver driver)
            : base(TimeSpan.FromSeconds(10))
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            var loaderDisplayed = driver.FindElements(Loader).FirstOrDefault().IsDisplayed();
            var modalDisplayed = driver.FindElements(ModalElements).Any();
            // CloseButton check prevents the situation when Modal elements have been loaded but CloseButton is not visible 
            // CloseButton will be checked after Modal elements are visible and Loader is not visible
            return !loaderDisplayed && modalDisplayed && CloseButton.Displayed;
        }

        protected override void ExecuteLoad() { }

        public void Close()
        {
            CloseButton.Click();
            //Wait until modal disappeared
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Message = "Shows modal didn't disappear after click CloseButton";
            wait.Until(d => !d.FindElements(ModalElements).Any());
        }

    }
}
