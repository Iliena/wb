﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Legal : LoadableComponent<Legal>
    {
        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//h2")]
        [Copy]
        IWebElement Caption { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//p")]
        [Copy]
        IList<IWebElement> Paragraphs { get; set; }

        IWebDriver driver;
        Uri uri;

        public Legal(IWebDriver driver, string url)
        {
            this.uri = new UriBuilder(url).Uri;
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.Url.EndsWith("legal") && Caption.IsDisplayed();
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(uri.AbsoluteUri);
        }
    }
}
