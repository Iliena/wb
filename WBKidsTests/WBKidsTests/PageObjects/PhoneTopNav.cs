﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class PhoneTopNav : LoadableComponent<PhoneTopNav>
    {
        [FindsBy(How = How.XPath, Using = "//*[@id='wbk-phone-top-nav']//*[@class='toggle-nav']")]
        IWebElement ToggleNav { get; set; }

        static By ToggleNavLocator = By.XPath("//*[@id='wbk-phone-top-nav']//*[@class='toggle-nav']");
        static By MainElementLocator = By.Id("wbk-phone-top-nav");

        private IWebDriver driver;
        public PhoneTopNav(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.FindElements(MainElementLocator).Any();
        }

        protected override void ExecuteLoad()
        {
            // nothing
        }

        public PhoneLeftNav OpenLeftNav()
        {
            var size = driver.Manage().Window.Size;
            if (size.Width > 700)
                driver.Manage().Window.Size = new System.Drawing.Size(700, size.Height - 50);

            driver.WaitElement(ToggleNavLocator, TimeSpan.FromSeconds(10));
            driver.WaitForPageLoad();
            driver.WaitElement(ToggleNavLocator, TimeSpan.FromSeconds(10)).Click();

            return new PhoneLeftNav(driver, size).Load();
        }
    }
}
