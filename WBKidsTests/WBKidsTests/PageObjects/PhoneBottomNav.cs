﻿using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class PhoneBottomNav : SlowLoadableComponent<PhoneBottomNav>
    {
        [FindsBy(How = How.XPath, Using = "//*[@id='wbk-phone-bottom-nav-inner']//h2")]
        [Copy]
        IWebElement Caption { get; set; }

        private IWebDriver driver;

        public PhoneBottomNav(IWebDriver driver) : base(TimeSpan.FromSeconds(4))
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Caption.IsDisplayed();
        }

        protected override void ExecuteLoad()
        {
            var size = driver.Manage().Window.Size;
            if (size.Width > 700)
                driver.Manage().Window.Size = new System.Drawing.Size(700, size.Height - 50);
        }
    }
}
