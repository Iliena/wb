﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class PrivacyPolicy : LoadableComponent<PrivacyPolicy>
    {
        IWebDriver driver;
        Uri uri;

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//p")]
        [Copy]
        IList<IWebElement> Paragraphs { get; set; }

        By MainElement = By.XPath("//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//p");

        public PrivacyPolicy(IWebDriver driver, string url)
        {
            this.driver = driver;
            this.uri = new UriBuilder(url).Uri;
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.Url.EndsWith("privacy-policy") && driver.FindElements(MainElement).Any();
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(uri.AbsoluteUri);
            PageFactory.InitElements(driver, this);
        }



    }
}
