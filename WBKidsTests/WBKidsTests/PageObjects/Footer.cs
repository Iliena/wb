﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;
using Fuel.QA.Selenium.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Footer : LoadableComponent<Footer>
    {
        // LEGAL/PRIVACY Footer
        //*[@id="dnn_ctr1088_HtmlModule_lblContent"]/h2
        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_HtmlModule_lblContent')]/h2")]
        [Copy]
        IWebElement Caption { get; set; }

        // Footer links: TERMS OF USE | PRIVACY POLICY | CONTACT US | OTHER
        //*[@id="dnn_ctr1087_HtmlModule_lblContent"]/ul/li/a
        //*[@id="dnn_ctr2104_HtmlModule_lblContent"]/ul/li[1]/a
        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_HtmlModule_lblContent')]/ul/li/a")]
        [Copy]
        IList<IWebElement> Links { get; set; }


        // Footer text
        //*[@id="dnn_ctr1087_HtmlModule_lblContent"]/p
        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_HtmlModule_lblContent')]/p")]
        [Copy]
        IWebElement Text { get; set; }

        // Close button
        //*[@id="modal-legal"]/div[1]/div[2]/div[3]
        [FindsBy(How = How.XPath, Using = "//div[@id='modal-legal']//div[contains(@class, 'wbk-modal-close')]")]
        IWebElement CloseButton { get; set; }


        static By MainElementLocator = By.Id("modal-legal");

        private IWebDriver driver;
        public Footer(IWebDriver driver)
        {
            this.driver = driver;
        }

        protected override bool EvaluateLoadedStatus()
        {
            PageFactory.InitElements(driver, this);
            return driver.FindElements(MainElementLocator).Any();
        }

        protected override void ExecuteLoad()
        {
            // nothing
        }

        public void Close()
        {
            CloseButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Message = "Languages modal didn't disappear after click CloseButton";
            wait.Until(d => !d.FindElements(MainElementLocator).Any());
        }

        public string GetTermsLink()
        {
            return Links[0].GetAttribute("href");
        }

        public string GetPrivacyPolicyLink()
        {
            return Links[1].GetAttribute("href");
        }

        public string GetContactUsLink()
        {
            return Links[2].GetAttribute("href");
        }

        public string GetLegalLink()
        {
            return Links[3].GetAttribute("href");
        }
    }
}
