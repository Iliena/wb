﻿using System;
using System.Collections.Generic;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    class ShowsPage : SlowLoadableComponent<ShowsPage>
    {
        private IWebDriver _driver;
        private Uri _showUri;
        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_TopLeftPane']//div[ contains(@class, 'DNNCharacter') and (contains (@class, 'DescLooney') or contains (@class, 'Description'))]")]
        [Copy]
        private IWebElement _characterDescription;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'DNNGallery') and contains(@class, 'DNNGalleryRelated')]//h3 | //div[@id='dnn_ContentPane']//h3")]
        [Copy]
        private IWebElement _characterTitle;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class, 'gallery-filters') and contains(@class, 'wbk-filters')]/li")]
        [Copy]
        private IList<IWebElement> _galeryFiltersButtons;

        [FindsBy(How = How.XPath, Using = "//figure//figcaption")]
        [Copy]
        private IList<IWebElement> _figureCaptions;
        [FindsBy(How = How.XPath, Using = "//div[@class='feed-nav']//span[@class='text']")]
        [Copy]
        private IList<IWebElement> _moreButtons;

        
        public ShowsPage(IWebDriver driver, string showurl)
            : base(TimeSpan.FromSeconds(10))
        {
            _driver = driver;
            _showUri = new UriBuilder(showurl).Uri;
            PageFactory.InitElements(_driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return _characterDescription.Displayed && _showUri.AbsolutePath.Contains(new UriBuilder(_driver.Url).Uri.AbsolutePath);
        }

        protected override void ExecuteLoad()
        {
            _driver.Navigate().GoToUrl(_showUri);
        }
    }
}
