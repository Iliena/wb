﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

using Fuel.QA.Selenium.Extensions;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class IsotopeElement
    {

        [Copy]
        public IWebElement Title { get { return element.FindElement(TitleLocator); } }

        [Copy]
        public IWebElement Subtitle { get { return element.FindElement(SubtitleLocator); } }

        public string Href
        {
            get
            {
                try
                {
                    return element.FindElement(HrefLocator).GetAttribute("href");
                }
                catch (NoSuchElementException)
                {
                    return string.Empty;
                }
            }
        }


        public Uri Url { get { return new UriBuilder(this.Href).Uri; } }

        static By TitleLocator = By.XPath(".//div/div/a/figure/figcaption/h3");
        static By SubtitleLocator = By.XPath(".//div/div/a/figure/figcaption/p");
        static By HrefLocator = By.XPath(".//div/div/a");

        IWebElement element;

        public IsotopeElement(IWebElement element)
        {
            this.element = element;
        }
    }
}
