﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium.Support.PageObjects;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class BottomNav : LoadableComponent<BottomNav>
    {
        // LEGAL/PRIVACY button
        //*[@id="dnn_ctr760_View_legalMenu"]
        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_View_legalMenu')]")]
        [Copy]
        IWebElement LegalButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_View_languageMenu')]")]
        [Copy]
        IWebElement LanguageButton { get; set; }

        static By MainElementLocator = By.Id("wbk-desktop-bottom-nav");

        private IWebDriver driver;
        public BottomNav(IWebDriver driver)
        {
            this.driver = driver;
        }

        protected override bool EvaluateLoadedStatus()
        {
            PageFactory.InitElements(driver, this);
            return driver.FindElements(MainElementLocator).Any();
        }

        protected override void ExecuteLoad()
        {
            // nothing
        }

        public void OpenLegal()
        {
            LegalButton.Click();
            By CloseButtonLocator = By.XPath("//div[@id='modal-legal']//div[contains(@class, 'wbk-modal-close')]");
            var CloseButton = driver.WaitElement(CloseButtonLocator, TimeSpan.FromSeconds(10));
            new DefaultWait<IWebElement>(CloseButton)
            {
                Timeout = TimeSpan.FromSeconds(10),
                Message = "Footer expand timed out"
            }.Until(e => e.IsImmovable());
        }

        public void OpenLanguage()
        {
            LanguageButton.Click();
            var closeButton = driver.WaitElement(By.ClassName("wbk-modal-close"), TimeSpan.FromSeconds(10));
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(d =>
            {
                UnableToLoadMessage = "Language expand timeout";
                return closeButton.IsImmovable();
            });
       
        }
    }
}
