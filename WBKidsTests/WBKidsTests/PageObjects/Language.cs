﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Language : SlowLoadableComponent<Language>
    {
        IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_ModalTitle']//h2")]
        [Copy]
        IWebElement Title { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_ModalItems']//ul//a")]
        [Copy]
        IList<IWebElement> Links { get; set; }

        [FindsBy(How = How.ClassName, Using = "wbk-modal-close")]
        IWebElement CloseButton { get; set; }

        static By Loader = By.Id("wbk-busy");
        static By ModalMainElement = By.XPath("//div[@id='dnn_ModalItems']");


        public Language(IWebDriver driver) : base(TimeSpan.FromSeconds(10)) 
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus() 
        {
            var loaderDisplayed = driver.FindElements(Loader).FirstOrDefault().IsDisplayed();
            var modalDisplayed = driver.FindElements(ModalMainElement).Any();
            return !loaderDisplayed && modalDisplayed;
        }

        protected override void ExecuteLoad() { }

        public void Close() 
        {
            CloseButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Message = "Languages modal didn't appear after click CloseButton";
            wait.Until(d => !d.FindElements(ModalMainElement).Any());
        }

    }
}
