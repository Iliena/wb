﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium.Support.PageObjects;
using Fuel.QA.Selenium.Extensions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Internal;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Characters : SlowLoadableComponent<Characters>
    {
        [FindsBy(How = How.XPath, Using = "//*[@id='search-character']/input")]
        IWebElement SearchInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='bxslider characters-menu']//a")]
        IList<IWebElement> CharactersUrls { get; set; }

        [Copy]
        [FindsBy(How = How.XPath, Using = "//*[@class='bxslider characters-menu']//p[@class='title-character']")]
        IList<IWebElement> CharactersTitles { get; set; }

        [Copy]
        [FindsBy(How = How.XPath, Using = "//*[@class='bxslider characters-menu']//p[@class='title-property']")]
        IList<IWebElement> CharactersProperties { get; set; }

        [Copy]
        IWebElement SearchInputPlaceholder
        {
            get
            {
                IWebElement wrappedElement = SearchInput;
                if (SearchInput as IWrapsElement != null)
                    wrappedElement = ((IWrapsElement)SearchInput).WrappedElement;

                var placeholder = wrappedElement.GetAttribute("placeholder");
                ((IJavaScriptExecutor)driver).ExecuteScript(
                    "arguments[0].textContent=arguments[1];",
                    wrappedElement, placeholder);
                //driver.ExecuteJavaScript<string>("arguments[0].textContent=arguments[1];return ''", SearchInput, placeholder);
                return SearchInput;
            }
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='modal-character']//h2")]
        [Copy]
        IWebElement Caption { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='modal-character']//div[@class='wbk-modal-close']")]
        IWebElement CloseButton { get; set; }

        static By MainElementLocator = By.Id("modal-character");
        private IWebDriver driver;
        public Characters(IWebDriver driver)
            : base(TimeSpan.FromSeconds(10))
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }
        protected override bool EvaluateLoadedStatus()
        {
            var loaderDisplayed = driver.FindElements(By.Id("wbk-busy")).FirstOrDefault().IsDisplayed();
            var mailElementDisplayed = driver.FindElements(MainElementLocator).Any();
            return mailElementDisplayed && !loaderDisplayed;
        }

        protected override void ExecuteLoad()
        {
            // nothing
        }

        public void Close()
        {
            CloseButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Message = "Shows modal didn't disappear after click CloseButton";
            wait.Until(d => !d.FindElements(MainElementLocator).Any());
        }

        public List<String> GetCharectersUrls()
        {
            return CharactersUrls.Select(e => e.GetAttribute("href")).ToList();
        }

    }
}
