﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class ContactUs : LoadableComponent<ContactUs>
    {
        IWebDriver driver;
        Uri uri;

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//h2")]
        [Copy]
        IList<IWebElement> Titles { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//p")]
        [Copy]
        IList<IWebElement> Paragraphs { get; set; }

        By MainElement = By.XPath("//div[@class='DNNSiteGeneric ']//div[contains(@id,'ContentPane')]//h2");

        public ContactUs(IWebDriver driver, string url)
        {
            
            this.uri = new UriBuilder(url).Uri;
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.Url.EndsWith("contact") && driver.FindElements(MainElement).Any();
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(uri.AbsoluteUri);
        }


    }
}
