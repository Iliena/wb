﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class DetailPage : LoadableComponent<DetailPage>
    {
        //*[@id="dnn_ctr1026_ModuleContent"]/div/div[1]/div[1]/div[2]/h2
        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_ModuleContent')]/div/div[1]/div[1]/div[2]/h2")]
        [Copy]
        IWebElement Caption { get; set; }

        //*[@id="dnn_ctr1026_ModuleContent"]/div/div[1]/div[1]/p
        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'dnn_ctr') and contains(@id, '_ModuleContent')]/div/div[1]/div[1]/p")]
        [Copy]
        IWebElement Description { get; set; }

        //*[@id="dnn_ctr1031_Fuel.Dnn.Modules.WbKidsGallery_UP"]/div[1]/div/h2
        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_RelatedPane']//*[contains(@id, 'dnn_ctr') and contains(@id, '_Fuel.Dnn.Modules.WbKidsGallery_UP')]/div[1]/div/h2")]
        [Copy]
        IWebElement RelatedCaption { get; set; }


        // All
        //*[@id="dnn_ctr1028_Fuel.Dnn.Modules.WbKidsGallery_UP"]/div[1]/div/h2
        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_GalleriesPane']//*[contains(@id, 'dnn_ctr') and contains(@id, '_Fuel.Dnn.Modules.WbKidsGallery_UP')]/div[1]/div/h2")]
        [Copy]
        IWebElement GalleriesCaption { get; set; }

        // Filters
        //*[@id="dnn_ctr1028_Fuel.Dnn.Modules.WbKidsGallery_UP"]/div[1]/div/div[1]/ul/li[1]/a/span[2]
        //*[@id="dnn_ctr1028_Fuel.Dnn.Modules.WbKidsGallery_UP"]/div[1]/div/div[1]/ul/li[2]/a/span[2]
        //*[@id="dnn_ctr1028_Fuel.Dnn.Modules.WbKidsGallery_UP"]/div[1]/div/div[1]/ul/li[3]/a/span[2]
        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_GalleriesPane']//*[contains(@id, 'dnn_ctr') and contains(@id, '_Fuel.Dnn.Modules.WbKidsGallery_UP')]/div[1]/div/div[1]/ul/li")]
        [Copy]
        IList<IWebElement> FilterButtons { get; set; }

        // Show More
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'feed-nav')]/a/span[@class='text']")]
        [Copy]
        IList<IWebElement> ShowMoreButtons { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='downloadSmall' and not(span)]")]
        [Copy(Id = "downloadSmall")]
        IWebElement DownloadButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'comic-buttons')]/a")]
        [Copy("ReadButton")]
        IWebElement ReadButton { get; set; }

        private IWebDriver driver;
        private Uri url;
        public DetailPage(IWebDriver driver, Uri url)
        {
            this.driver = driver;
            this.url = url;
        }

        public DetailPage(IWebDriver driver, string url)
        {
            this.driver = driver;
            this.url = new UriBuilder(url).Uri;
        }

        protected override bool EvaluateLoadedStatus()
        {
            return new Uri(driver.Url) == url;
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(url);
            PageFactory.InitElements(driver, this);
        }
    }
}
