﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Gallery : LoadableComponent<Gallery>
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'DNNGallery')]//div[contains(@class,'gallery-bg')]/div[contains(@class,'wbk-container')]/h2")]
        [Copy]
        IWebElement Caption { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='wbk-container']/div[contains(@class, 'clearfix')]/ul[contains(@class, 'gallery-filters')]/li")]
        [Copy]
        IList<IWebElement> FilterButtons { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'DNNGallery')]//div[contains(@class,'wbk-container')]/div/ul[contains(@class,'feed')]/li//figure/figcaption/p")]
        [Copy]
        IList<IWebElement> Figcaptions { get; set; }


        IWebDriver driver;
        Uri url;

        public Gallery(IWebDriver driver, string url)
        {
            this.driver = driver;
            this.url = new UriBuilder(url).Uri;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return new UriBuilder(driver.Url).Path == this.url.AbsolutePath && Caption.IsDisplayed();
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(url);
        }
    }
}
