﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class PageNotFound : LoadableComponent<PageNotFound>
    {
        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_ContentPane']//h3")]
        [Copy]
        IWebElement Caption {get; set;}

        [FindsBy(How = How.XPath, Using = "//*[@id='dnn_ContentPane']//p")]
        [Copy]
        IWebElement Content { get; set; }

        [FindsBy(How = How.XPath, Using = "//img[contains(@src,'-404.png')]")]
        IWebElement CharacterBodyShot { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'_HtmlModule_lblContent')]//div/p[1]/a[@href='/']")]
        [Copy]
        IWebElement HomepageLink { get; set; }


        IWebDriver driver;
        string host;
        public PageNotFound(IWebDriver driver, string host)
        {
            this.driver = driver;
            this.host = host;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return Caption.IsDisplayed() && HomepageLink.IsDisplayed();
        }

        protected override void ExecuteLoad()
        {
            var builer = new UriBuilder(host);
            builer.Path = "PageNotFound42";
            driver.Navigate().GoToUrl(builer.Uri);
        }
    }
}
