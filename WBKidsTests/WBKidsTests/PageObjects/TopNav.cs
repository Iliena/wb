﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class TopNav : LoadableComponent<TopNav>
    {
        //*[@id="dnn_ctr761_View_showsMenuHome"]
        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'_View_showsMenu')]")]
        [Copy]
        public IWebElement Shows { get; set; }

        //*[@id="dnn_ctr761_View_charactersMenuHome"]
        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'_View_charactersMenu')]")]
        [Copy]
        IWebElement Characters { get; set; }

        //*[@id="dnn_ctr761_View_sitesMenuHome"]
        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'_View_sitesMenu')]")]
        [Copy]
        IWebElement Sites { get; set; }

        // By MainElementLocator = By.Id("global-nav-home");
        By MainElementLocator = By.Id("dnn_SiteTopNav");

        IWebDriver driver;
        public TopNav(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.FindElements(MainElementLocator).Any();
        }

        protected override void ExecuteLoad()
        {
            // nothing
        }
        public Characters OpenCharacters()
        {
            Characters.Click();
            return new Characters(driver).Load();
        }

        public Shows OpenShows()
        {
            Shows.Click();
            return new Shows(driver).Load();
        }

        public WBKidsSites OpenWBKidsSites()
        {
            Sites.Click();
            return new WBKidsSites(driver).Load();
        }
    }
}
