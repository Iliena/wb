﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Fuel.QA.WBKidsTests.Copy;
namespace Fuel.QA.WBKidsTests.PageObjects
{
    class ExpressCookieDialog : SlowLoadableComponent<ExpressCookieDialog>
    {
        private IWebDriver _driver;

        private static By _dialogInfo = By.XPath("//div[@id='banner-cookie-implied']");

        [FindsBy(How = How.XPath, Using = "//div[@class='wbk-baner-close']")]
        private IWebElement _closedialog = null;

        [FindsBy(How = How.XPath, Using = "//div[@class='wbk-banner-content']/h2")]
        [Copy]
        private IWebElement _contentTitle;
        
        [FindsBy(How = How.XPath, Using = "//div[@class='wbk-banner-content']/p")]
        [Copy]
        private IWebElement _content;

        public ExpressCookieDialog(IWebDriver driver)
            : base(TimeSpan.FromSeconds(10))
        {
            _driver = driver;
            PageFactory.InitElements(this._driver, this);
        }

        public static bool IsDialogPresent(IWebDriver driver)
        {
            return driver.FindElement(_dialogInfo).Displayed; ;
        }
        public void ClickClose()
        {
            Console.WriteLine("Clicking close button on express cookie");
            _closedialog.Click();
        }
        protected override bool EvaluateLoadedStatus()
        {
            return _driver.FindElement(_dialogInfo).Displayed;
        }

        protected override void ExecuteLoad()
        {
        }
    }
}
