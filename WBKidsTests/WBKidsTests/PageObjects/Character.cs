﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium.Support.PageObjects;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class Character : LoadableComponent<Character>
    {
        IWebDriver driver;
        Uri url;

        [FindsBy(How = How.XPath, Using = "//*[@id='dnn_TopLeftPane']//h3//..")]
        [Copy]
        IWebElement CharacterDescription { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='dnn_TopLeftPane']//h3/span")]
        [Copy]
        IWebElement MoreFrom { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='dnn_RelatedPane']//h3")]
        [Copy]
        IWebElement OtherCharacters { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'DNNGalleryVideo')]//h2")]
        [Copy]
        IWebElement Videos { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class, 'DNNGalleryGame')]//h2")]
        [Copy]
        IWebElement Games { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class, 'DNNGalleryComic')]//h2")]
        [Copy]
        IWebElement Comics { get; set; }


        [FindsBy(How = How.XPath, Using = "//*[contains(@class, 'DNNGalleryDownload')]//h2")]
        [Copy]
        IWebElement Downloads { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_GalleriesPane']//*[contains(@id, 'dnn_ctr') and contains(@id, '_Fuel.Dnn.Modules.WbKidsGallery_UP')]/div[1]/div/div[1]/ul/li")]
        [Copy]
        IList<IWebElement> FilterButtons { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'feed-nav')]/a/span[@class='text']")]
        [Copy]
        IList<IWebElement> ShowMoreButtons { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='aspect-ratio']//figcaption")]
        [Copy]
        IList<IWebElement> GalleryItems { get; set; }
        
        public Character(IWebDriver driver, Uri url)
        {
            this.driver = driver;
            this.url = url;
            PageFactory.InitElements(driver, this);
        }

        public Character(IWebDriver driver, string url)
        {
            this.driver = driver;
            this.url = new UriBuilder(url).Uri;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return url.AbsolutePath.Contains(new Uri(driver.Url).AbsolutePath);
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(url);
        }
    }
}
