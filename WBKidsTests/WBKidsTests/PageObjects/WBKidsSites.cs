﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.Selenium.Extensions;
using Fuel.QA.WBKidsTests.Copy;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    public class WBKidsSites : SlowLoadableComponent<WBKidsSites>
    {
        private IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//div[@id='dnn_ModalTitle']//div[@class='Normal']/h2")]
        [Copy]
        IWebElement WBKidsSitesTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='wbk-modal-outer']//div[@class='wbk-modal-close']")]
        IWebElement CloseButton { get; set; }

        static By Loader = By.Id("wbk-busy");
        static By ModalElements = By.XPath("//div[@class='wbk-modal-outer']//ul//li");

        public WBKidsSites(IWebDriver driver)
            : base(TimeSpan.FromSeconds(10))
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            var loaderDisplayed = driver.FindElements(Loader).FirstOrDefault().IsDisplayed();
            var modalDisplayed = driver.FindElements(ModalElements).Any();
            // CloseButton check prevents the situation when Modal elements have been loaded but CloseButton is not visible 
            // CloseButton will be checked after Modal elements are visible and Loader is not visible
            return !loaderDisplayed && modalDisplayed && CloseButton.Displayed;
        }

        protected override void ExecuteLoad() { }

        public void Close()
        {
            CloseButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Message = "Shows modal didn't disappear after click CloseButton";
            wait.Until(d => !d.FindElements(ModalElements).Any()); 
        }

    }
}
