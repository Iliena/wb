﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Fuel.QA.Selenium.Extensions;
using Fuel.QA.Translators;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Fuel.QA.WBKidsTests.PageObjects
{
    class ImpliedCookieDialog : SlowLoadableComponent<ImpliedCookieDialog>
    {
        private static By _modalDialogLocator = By.XPath("//div[@id='modal-cookie-express']//div[@class='modal-dialog']");

        [FindsBy(How = How.XPath, Using = "//span[contains(@class,'modal-button') and contains(@class, 'agree')]")]
        [Copy]
        private IWebElement _agreeButton;

        [FindsBy(How = How.XPath, Using = "//span[contains(@class,'modal-button') and contains(@class, 'more')]")]
        [Copy]
        private IWebElement _MoreInfoButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-body']/h3")]
        [Copy]
        private IWebElement _contentTitle;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-body']//p")]
        [Copy]
        private IList<IWebElement> _content;

        private IWebDriver _driver;

        public ImpliedCookieDialog(IWebDriver driver)
            : base(TimeSpan.FromSeconds(10))
        {
            this._driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public static bool IsDialogPresent(IWebDriver driver)
        {
            return driver.FindElement(_modalDialogLocator).Displayed;
        }

        public void ClickAccept()
        {
            Console.WriteLine("Clicking accept button");
            _agreeButton.Click();
        }

        protected override bool EvaluateLoadedStatus()
        {
            return _driver.FindElement(_modalDialogLocator).Displayed;
        }
        protected override void ExecuteLoad()
        { }
    }


}
