﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Fuel.QA.WBKidsTests
{
    public class TestCaseFactory
    {
        public static IEnumerable PhoneLeftNavTestSource
        {
            get { return MakeTestSource("Phone Left Nav"); }
        }
        public static IEnumerable ButtomNavTestSource
        {
            get { return MakeTestSource("Buttom Nav"); }
        }
        public static IEnumerable LegalFooterTestSource
        {
            get { return MakeTestSource("Legal Footer"); }
        }
        public static IEnumerable LanguageFooterTestSource
        {
            get { return MakeTestSource("Language Footer"); }
        }
        public static IEnumerable TopNavTestSource
        {
            get { return MakeTestSource("Top Nav"); }
        }
        public static IEnumerable GalleryTestSource
        {
            get { return MakeTestSource("Gallery"); }
        }
        public static IEnumerable DetailTestSource
        {
            get { return MakeTestSource("Detail"); }
        }
        public static IEnumerable TermsTestSource
        {
            get { return MakeTestSource("Terms"); }
        }
        public static IEnumerable PrivacyPolicyTestSource
        {
            get { return MakeTestSource("Privacy Policy"); }
        }
        public static IEnumerable ContactUsTestSource
        {
            get { return MakeTestSource("Contact Us"); }
        }
        public static IEnumerable LegalTestSource
        {
            get { return MakeTestSource("Legal"); }
        }
        public static IEnumerable PageNotFoundTestSource
        {
            get { return MakeTestSource("Page Not Found"); }
        }
        public static IEnumerable ImpliedCookieLocTestSource
        {
            get
            {
                return MakeSelectiveLocalesTestSource("Localization implied cookie dialog",
                    CookieHandling.GetLocalesThatNeedCookies(CookieType.Implied));
            }
        }
        public static IEnumerable ExpressCookieLocTestSource
        {
            get
            {
                return MakeSelectiveLocalesTestSource("Localization express cookie dialog",
                    CookieHandling.GetLocalesThatNeedCookies(CookieType.Express));
            }
        }

        public static IEnumerable ShowsTestSource
        {
            get
            {
                return MakeTestSource("Shows page");
            }
        }

        public static IEnumerable MakeTestSource(string testPage)
        {
            var sites = System.Environment.GetEnvironmentVariable("WBKIDSENV");
            if (sites == null)
                sites = ConfigurationManager.AppSettings["WBKIDSENV"];

            var regions = System.Environment.GetEnvironmentVariable("WBKIDSREGIONS");
            if (regions == null)
                regions = ConfigurationManager.AppSettings["WBKIDSREGIONS"];

            foreach (var site in sites.Split(','))
            {
                foreach (var region in regions.Split(','))
                {
                    var lng = region.Split('-')[0];
                    var name = site.Replace('.', '-') + "." + region + "." + testPage;
                    yield return new TestCaseData(site, region, lng).SetName(name);
                }
            }
        }
        public static IEnumerable MakeSelectiveLocalesTestSource(string testName, List<string> testlocales)
        {
            var sites = System.Environment.GetEnvironmentVariable("WBKIDSENV");
            if (sites == null)
                sites = ConfigurationManager.AppSettings["WBKIDSENV"];

            foreach (var site in sites.Split(','))
            {
                foreach (var region in testlocales)
                {
                    var lng = region.Split('-')[0];
                    var name = site.Replace('.', '-') + "." + region + "." + testName;
                    yield return new TestCaseData(site, region, lng).SetName(name);
                }
            }
        }

    }
}
