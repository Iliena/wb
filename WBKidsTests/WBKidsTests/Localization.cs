﻿using Fuel.QA.Selenium;
using Fuel.QA.TeamCity;
using Fuel.QA.Translators;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.Selenium.Extensions;
using Fuel.QA.Core;
using System.Collections;
using System.Configuration;
using Fuel.QA.WBKidsTests.PageObjects;
using Fuel.QA.WBKidsTests.Copy;
using OpenQA.Selenium.Firefox;

namespace Fuel.QA.WBKidsTests
{
    [TestFixture]
    public class Localization
    {
        IWebDriver driver;
        SimpleDetector simpleDetector;
        List<ILanguageDetector> translators = new List<ILanguageDetector>();
        Dictionary<string, string> detectErrors = new Dictionary<string, string>();
        Logger logger = new Logger();

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            simpleDetector = new SimpleDetector("SimpleDetectorData.xml");

            var yatr = new YandexTranslator("trnsl.1.1.20150128T141917Z.d21ba982b7cdef76.9f6bf6310fc306ba7e34d39529f2f3269a5be548");
            var mstr = new MSTranslator("abozhko", "27M2rdyhh/prjOq6azPTFpu6iJ2Przzqvef/jymWBK8=");

            translators.Add(new CacheableLanguageDetector(yatr));
            translators.Add(new CacheableLanguageDetector(mstr));
        }

        [SetUp]
        public void SetUp()
        {
            var drv = new ChromeDriver();
            //var drv = new FirefoxDriver();
            driver = new LoggableWebDriver(drv);
            driver.Manage().Window.Maximize();
        }


        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Status == TestStatus.Failed)
            {
                logger.SchedulePublishing(Utils.CaptureScreen(true), TestContext.CurrentContext.Test.Name + "_err");
                Console.WriteLine("Current URL: " + driver.Url);
            }

            var msg = string.Format("Wrong language found at {0} strings:\n{1}\n",
                detectErrors.Count, string.Join("\n", detectErrors));
            Console.WriteLine(msg);

            logger.PublishQueue();
            driver.Stop();
            detectErrors.Clear();
        }


        public void TestPageElement(object pageElement, string expectedLng)
        {
            var elements = CopyFactory.GetElements(pageElement);
            Console.WriteLine("\nThere are {0} copy elements in {1}", elements.Count, pageElement.GetType());
            Console.WriteLine(driver.Url);

            foreach (var elem in elements)
            {
                var str = elem.Text;
                if (string.IsNullOrWhiteSpace(str))
                    continue;

#if DEBUG
                elem.TryHighlight();
#endif

                if (simpleDetector.Test(str, expectedLng))
                {
                    Console.WriteLine("{0}\n  Expected: {1}\n  Detected: {2} (via SimpleDetector)", str, expectedLng, expectedLng);
                    continue;
                }

                var detected = translators.Select(t => t.Detect(str));
                var detected_string = string.Join(", ", detected);
                if (!detected.Contains(expectedLng))
                {
                    detectErrors[str] = detected_string;
                    //logger.SchedulePublishing(elem.Snapshot(), "name");
                    elem.TryHighlight();
                }

                Console.WriteLine("{0}\n  Expected: {1}\n  Detected: {2}", str, expectedLng, detected_string);
            }
        }

        [Test, TestCaseSource(typeof(TestCaseFactory),"PhoneLeftNavTestSource")]
        public void PhoneLeftNavTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);
            var phoneLeftNav = new PhoneTopNav(driver).Load().OpenLeftNav();
            phoneLeftNav.ClickLegalNav();
            TestPageElement(phoneLeftNav, lng);
            phoneLeftNav.Close();

            AssertDetectedErrors();
        }
       
        [Test, TestCaseSource(typeof(TestCaseFactory),"ButtomNavTestSource")]
        public void ButtomNavTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);
            var bottomNav = new BottomNav(driver).Load();
            TestPageElement(bottomNav, lng);

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "LegalFooterTestSource")]
        public void LegalFooterTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);
            var bottomNav = new BottomNav(driver).Load();
            bottomNav.OpenLegal();
            var footer = new Footer(driver).Load();
            TestPageElement(footer, lng);
            footer.Close();

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "LanguageFooterTestSource")]
        public void languageFooterTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);
            var bottomNav = new BottomNav(driver).Load();
            bottomNav.OpenLanguage();
            var language = new Language(driver).Load();
            TestPageElement(language, lng);
            language.Close();

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "TopNavTestSource")]
        public void TopNavTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);
            var topNav = new TopNav(driver).Load();
            TestPageElement(topNav, lng);
            var caracters = topNav.OpenCharacters();
            TestPageElement(caracters, lng);
            caracters.Close();

            topNav = new TopNav(driver).Load(); // ??
            var shows = topNav.OpenShows();
            TestPageElement(shows, lng);
            shows.Close();

            topNav = new TopNav(driver).Load(); // ??
            var sites = topNav.OpenWBKidsSites();
            TestPageElement(shows, lng);
            sites.Close();

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "GalleryTestSource")]
        public void GalleryTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);
            var galleryUrls = homePage.GetGalleryLinks();
            Console.WriteLine("detailUrls:\n" + string.Join("\n", galleryUrls));

            foreach (var url in galleryUrls)
                TestPageElement(new Gallery(driver, url).Load(), lng);

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "DetailTestSource")]
        public void DetailsTest(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale);

            var detailUrls = homePage.GetDetailLinks();
            Console.WriteLine("detailUrls:\n" + string.Join("\n", detailUrls));

            foreach (var url in detailUrls)
            {
                TestPageElement(new DetailPage(driver, url).Load(), lng);
                TestPageElement(new TopNav(driver).Load(), lng);
                TestPageElement(new BottomNav(driver).Load(), lng);
                //TestPageElement(new PhoneBottomNav(driver).Load(), lng);
                driver.Manage().Window.Maximize();
            }

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "TermsTestSource")]
        public void TermsTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            // Bottom Nav
            var bottomNav = new BottomNav(driver).Load();
            bottomNav.OpenLegal();

            // Footer
            var footer = new Footer(driver).Load();
            var termsLink = footer.GetTermsLink();

            // Terms page
            var termsPage = new Terms(driver, termsLink).Load();
            TestPageElement(termsPage, lng);

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "PrivacyPolicyTestSource")]
        public void PrivacyPolicyTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            var bottomNav = new BottomNav(driver).Load();
            bottomNav.OpenLegal();

            // Footer
            var footer = new Footer(driver).Load();
            var privacyPolicyLink = footer.GetPrivacyPolicyLink();

            var policyPage = new PrivacyPolicy(driver, privacyPolicyLink).Load();
            TestPageElement(policyPage, lng);

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "ContactUsTestSource")]
        public void ContactUsTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            var bottomNav = new BottomNav(driver).Load();
            bottomNav.OpenLegal();

            // Footer
            var footer = new Footer(driver).Load();
            var privacyPolicyLink = footer.GetPrivacyPolicyLink();

            var contactUsLink = footer.GetContactUsLink();

            var contactUsPage = new ContactUs(driver, contactUsLink).Load();
            TestPageElement(contactUsPage, lng);

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "LegalTestSource")]
        public void LegalTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            var bottomNav = new BottomNav(driver).Load();
            bottomNav.OpenLegal();

            // Footer
            var footer = new Footer(driver).Load();
            var privacyPolicyLink = footer.GetPrivacyPolicyLink();

            var legalLink = footer.GetLegalLink();

            TestPageElement(new Legal(driver, legalLink).Load(), lng);

            AssertDetectedErrors();
        }

        [Test, TestCaseSource(typeof(TestCaseFactory), "PageNotFoundTestSource")]
        public void PageNotFoundTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            TestPageElement(new PageNotFound(driver, host).Load(), lng);

            AssertDetectedErrors();
        }
        [Test, TestCaseSource(typeof(TestCaseFactory), "ImpliedCookieLocTestSource")]
        public void ImpliedCookieLocalizationTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            TestPageElement(new ImpliedCookieDialog(this.driver).Load(), lng);
            AssertDetectedErrors();
        }
        [Test, TestCaseSource(typeof(TestCaseFactory), "ExpressCookieLocTestSource")]
        public void ExpressCookieLocalizationTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            TestPageElement(new ExpressCookieDialog(this.driver).Load(), lng);
            AssertDetectedErrors();
        }
        [Test, TestCaseSource(typeof(TestCaseFactory), "ShowsTestSource")]
        public void ShowsTest(string host, string locale, string lng)
        {
            OpenHomePage(host, locale);
            string[] urls = new TopNav(driver).OpenShows().ShowUrls;
            foreach (var url in urls)
            {
                TestPageElement(new ShowsPage(driver, url).Load(), lng);
            }
            AssertDetectedErrors();

        }
        public void AssertDetectedErrors()
        {
            if (detectErrors.Any())
                Assert.Fail("Wrong language found at {0} strings:\n{1}\n",
                detectErrors.Count, string.Join("\n", detectErrors));
        }

        public HomePage OpenHomePage(string host, string locale)
        {
            var homePage = new HomePage(this.driver, host, locale).Load();
            //if (CookieHandling.NeedToAppear(CookieType.Implied, locale))
            //    new ImpliedCookieDialog(this.driver).Load().ClickAccept();
            //if (CookieHandling.NeedToAppear(CookieType.Express, locale))
            //    new ExpressCookieDialog(this.driver).Load().ClickClose();

            return homePage;
        }
    }
}
