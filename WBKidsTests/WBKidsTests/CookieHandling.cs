﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Fuel.QA.WBKidsTests.PageObjects;

namespace Fuel.QA.WBKidsTests
{
    public enum CookieType
    {
        Implied,
        Express
    };
    class CookieHandling
    {
        public static string FileName = "CookieDialog.xml";
        private static CookieDialogData _data;
        public static CookieDialogData Data
        {
            get { return _data; }
        }
        static CookieHandling()
        {
            Console.WriteLine("Reading xml " + FileName);
            XmlSerializer xsSerializer = new XmlSerializer(typeof(CookieDialogData));
            using (XmlReader reader = XmlReader.Create(FileName))
            {
                _data = (CookieDialogData)xsSerializer.Deserialize(reader);
                Console.WriteLine("................reading done");
            }
        }
        public static List<string> GetLocalesThatNeedCookies(CookieType type)
        {
            switch (type)
            {
                case CookieType.Implied:
                    return Data.Implied.Items.ToList();
                case CookieType.Express:
                    return Data.Express.Items.ToList();
            }
            return null;

        }
        public static bool NeedToAppear(CookieType type, string locale)
        {
            switch (type)
            {
                case CookieType.Express:
                    return Data.Express.Items.Any(item => item.Equals(locale));
                case CookieType.Implied:
                    return Data.Implied.Items.Any(item => item.Equals(locale));
            }
            throw new ArgumentException("Not recognized a cookie type " + type);
        }

    }

    [Serializable]
    [XmlRoot(ElementName = "CookieDialogData", IsNullable = false)]
    public class CookieDialogData
    {
        [XmlElement("Implied")]
        public CookieDialogDataItem Implied { get; set; }

        [XmlElement("Express")]
        public CookieDialogDataItem Express { get; set; }
    }

    [Serializable]
    public class CookieDialogDataItem
    {
        [XmlElement("Item")]
        public string[] Items { set; get; }
    }
}
