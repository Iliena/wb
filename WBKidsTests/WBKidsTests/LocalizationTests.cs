﻿using Fuel.QA.Selenium;
using Fuel.QA.Selenium.Extensions;
using Fuel.QA.TeamCity;
using Fuel.QA.Translators;
using Fuel.QA.WBKidsTests.Copy;
using Fuel.QA.WBKidsTests.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using Fuel.QA.Core;
using OpenQA.Selenium.Firefox;

namespace Fuel.QA.WBKidsTests
{
    [TestFixture, Explicit]
    public class LocalizationTests
    {
        IWebDriver driver;
        SimpleDetector simpleDetector;
        List<ILanguageDetector> translators = new List<ILanguageDetector>();
        Dictionary<string, string> detectErrors = new Dictionary<string, string>();
        Logger logger = new Logger();

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            simpleDetector = new SimpleDetector("SimpleDetectorData.xml");

            var yatr = new YandexTranslator("trnsl.1.1.20150128T141917Z.d21ba982b7cdef76.9f6bf6310fc306ba7e34d39529f2f3269a5be548");
            var mstr = new MSTranslator("abozhko", "27M2rdyhh/prjOq6azPTFpu6iJ2Przzqvef/jymWBK8=");

            translators.Add(new CacheableLanguageDetector(yatr));
            translators.Add(new CacheableLanguageDetector(mstr));
        }

        [SetUp]
        public void SetUp()
        {
            var drv = new FirefoxDriver();
            //var drv = new FirefoxDriver();
            driver = new LoggableWebDriver(drv);
            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Status == TestStatus.Failed)
            {
                logger.SchedulePublishing(Utils.CaptureScreen(true), TestContext.CurrentContext.Test.Name + "_err");
                Console.WriteLine("Current URL: " + driver.Url);
            }

            var msg = string.Format("Wrong language found at {0} strings:\n{1}\n",
                detectErrors.Count, string.Join("\n", detectErrors));
            Console.WriteLine(msg);

            detectErrors.Clear();
            logger.PublishQueue();
            driver.Stop();
        }

        public IEnumerable TestSource()
        {
            var sites = System.Environment.GetEnvironmentVariable("WBKIDSENV");
            if (sites == null)
                sites = ConfigurationManager.AppSettings["WBKIDSENV"];

            var regions = System.Environment.GetEnvironmentVariable("WBKIDSREGIONS");
            if (regions == null)
                regions = ConfigurationManager.AppSettings["WBKIDSREGIONS"];

            foreach (var site in sites.Split(','))
            {
                foreach (var region in regions.Split(','))
                {
                    var lng = region.Split('-')[0];
                    var name = site.Replace('.', ' ') + " | " + region;
                    yield return new TestCaseData(site, region, lng).SetName(name);
                }
            }
        }

        public void TestPageElement(object pageElement, string expectedLng)
        {
            var elements = CopyFactory.GetElements(pageElement);
            Console.WriteLine("\nThere are {0} copy elements in {1}", elements.Count, pageElement.GetType());
            Console.WriteLine(driver.Url);

            foreach (var elem in elements)
            {
                var str = elem.Text;
                if (string.IsNullOrWhiteSpace(str))
                    continue;

#if DEBUG
                elem.TryHighlight();
#endif

                if (simpleDetector.Test(str, expectedLng))
                {
                    Console.WriteLine("{0}\n  Expected: {1}\n  Detected: {2} (via SimpleDetector)", str, expectedLng, expectedLng);
                    continue;
                }

                var detected = translators.Select(t => t.Detect(str));
                var detected_string = string.Join(", ", detected);
                if (!detected.Contains(expectedLng))
                {
                    detectErrors[str] = detected_string;
                    //logger.SchedulePublishing(elem.Snapshot(), "name");
                    elem.TryHighlight();
                }

                Console.WriteLine("{0}\n  Expected: {1}\n  Detected: {2}", str, expectedLng, detected_string);
            }
        }

        [Test, TestCaseSource("TestSource"), Explicit]

        public void TestMethod(string host, string locale, string lng)
        {
            var homePage = OpenHomePage(host, locale); ;
            // Phone Left Nav
            var phoneLeftNav = new PhoneTopNav(driver).Load().OpenLeftNav();
            phoneLeftNav.ClickLegalNav();
            TestPageElement(phoneLeftNav, lng);
            phoneLeftNav.Close();
            

            // Bottom Nav
            var bottomNav = new BottomNav(driver).Load();
            TestPageElement(bottomNav, lng);
            bottomNav.OpenLegal();
            
            // Footer
            var footer = new Footer(driver).Load();
            TestPageElement(footer, lng);
            var termsLink = footer.GetTermsLink();
            var privacyPolicyLink = footer.GetPrivacyPolicyLink();
            var contactUsLink = footer.GetContactUsLink();
            var legalLink = footer.GetLegalLink();
            footer.Close();
            

            bottomNav.OpenLanguage();
            var language = new Language(driver).Load();
            TestPageElement(language, lng);
            language.Close();


            // Top Nav
            var topNav = new TopNav(driver).Load();
            TestPageElement(topNav, lng);
            var caracters = topNav.OpenCharacters();
            TestPageElement(caracters, lng);
            caracters.Close();

            topNav = new TopNav(driver).Load(); // ??
            var shows = topNav.OpenShows();
            TestPageElement(shows, lng);
            shows.Close();

            topNav = new TopNav(driver).Load(); // ??
            var sites = topNav.OpenWBKidsSites();
            TestPageElement(sites, lng);
            sites.Close();

            var galleryUrls = homePage.GetGalleryLinks();
            var detailUrls = homePage.GetDetailLinks();
            Console.WriteLine("detailUrls:\n" + string.Join("\n", galleryUrls));
            Console.WriteLine("galleryUrls:\n" + string.Join("\n", detailUrls));

            // Isotopes
            foreach (var isotope in homePage.GetIsotopes())
                TestPageElement(isotope, lng);

            // Gallary Pages
            foreach (var url in galleryUrls)
                TestPageElement(new Gallery(driver, url).Load(), lng);

            // Detail pages
            foreach (var url in detailUrls)
            {
                TestPageElement(new DetailPage(driver, url).Load(), lng);
                TestPageElement(new TopNav(driver).Load(), lng);
                TestPageElement(new BottomNav(driver).Load(), lng);
                //TestPageElement(new PhoneBottomNav(driver).Load(), lng);
                driver.Manage().Window.Maximize();
            }

            // Terms page
            var termsPage = new Terms(driver, termsLink).Load();
            TestPageElement(termsPage, lng);

            // Privacy Policy page
            var policyPage = new PrivacyPolicy(driver, privacyPolicyLink).Load();
            TestPageElement(policyPage, lng);

            // Contact Us Page
            var contactUsPage = new ContactUs(driver, contactUsLink).Load();
            TestPageElement(contactUsPage, lng);
            
            // Legal page
            TestPageElement(new Legal(driver, legalLink).Load(), lng);
            
            // Page Not Found
            TestPageElement(new PageNotFound(driver, host).Load(), lng);


            if (detectErrors.Any())
                Assert.Fail("Wrong language found at {0} strings:\n{1}\n",
                detectErrors.Count, string.Join("\n", detectErrors));
        }
        public HomePage OpenHomePage(string host, string locale)
        {
            var homePage = new HomePage(this.driver, host, locale).Load();
            if (CookieHandling.NeedToAppear(CookieType.Implied, locale))
                new ImpliedCookieDialog(this.driver).Load().ClickAccept();
            if (CookieHandling.NeedToAppear(CookieType.Express, locale))
                new ExpressCookieDialog(this.driver).Load().ClickClose();

            return homePage;
        }
    }
}
