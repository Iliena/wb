﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Translators
{
    public interface ILanguageDetector
    {
        string Detect(string str);
        string[] DetectArray(string[] texts);
    }
}
