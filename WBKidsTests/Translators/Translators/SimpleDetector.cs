﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace Fuel.QA.Translators
{
    public class SimpleDetector
    {
        private SimpleDetectorData data;
        public SimpleDetector(string FileName)
        {
            XmlSerializer ser = new XmlSerializer(typeof(SimpleDetectorData));
            using (XmlReader reader = XmlReader.Create(FileName))
            {
                data = (SimpleDetectorData)ser.Deserialize(reader);
            }
        }


        /// <summary>
        /// Tests whether given string is valid on given languages
        /// </summary>
        /// <param name="str">Text for test</param>
        /// <param name="languages">Language codes</param>
        /// <returns></returns>
        public bool Test(string str, params string[] languages)
        {
            var found = data.Text.Where(d => d.Value == str);
            return found.Any(d =>
            {
                var lngs = d.language.Split(',');
                foreach (var l in languages)
                {
                    if (lngs.Any(lang => lang == l))
                        return true;
                }
                return false;
            });
        }
    }

    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "SimpleDetectorData", IsNullable = false)]
    public class SimpleDetectorData
    {
        [XmlElementAttribute("Text")]
        public SimpleDetectorDataText[] Text { get; set; }
    }

    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class SimpleDetectorDataText
    {
        [XmlAttribute]
        public string language { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}
