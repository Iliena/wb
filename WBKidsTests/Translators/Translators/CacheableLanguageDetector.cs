﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Translators
{
    public class CacheableLanguageDetector : ILanguageDetector, IDisposable
    {
        private ILanguageDetector translator;
        private Dictionary<string, string> cache;

        public CacheableLanguageDetector(ILanguageDetector translator)
        {
            this.translator = translator;
            this.cache = new Dictionary<string, string>();
        }

        public string Detect(string str)
        {
            //Console.WriteLine("[CacheableLanguageDetector] Cache has {0} items", this.cache.Count);
            string output;
            if (!cache.TryGetValue(str, out output))
            {
                //Console.WriteLine("[CacheableLanguageDetector] Detecting string: {0}", str);
                output = translator.Detect(str);
                cache.Add(str, output);
            }
            else
            {
                //Console.WriteLine("[CacheableLanguageDetector] From cache: {0}", str);
            }
            return output;
        }

        public string [] DetectArray(params string[] texts)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            cache.Clear();
        }
    }
}
