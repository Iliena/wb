﻿using Fuel.QA.Translators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yandex.Translator;

namespace Fuel.QA.Translators
{
    public class YandexTranslator : ILanguageDetector
    {
        private IYandexTranslator translator;

        public YandexTranslator(string key)
        {
            this.translator = Yandex.Translator.Yandex.Translator(
                api => api.ApiKey(key).Format(ApiDataFormat.Json));
        }

        /// <summary>
        /// Makes a language detection request to Yandex.Translator web service.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Detect(string str)
        {
            string output = string.Empty;
            translator.Detect(str, out output);
            return output;
        }

        /// <summary>
        /// Yandex has no DetectArray. Cheating
        /// </summary>
        /// <param name="texts"></param>
        /// <returns></returns>
        public string[] DetectArray(params string[] texts)
        {
            var results = new string[texts.Length];
            for (var i = 0; i < texts.Length; i++)
                results[i] = this.Detect(texts[i]);
            return results;
        }
    }
}
