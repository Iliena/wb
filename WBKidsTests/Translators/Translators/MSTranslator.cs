﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yam.Microsoft.Translator;
using Yam.Microsoft.Translator.TranslatorService;

/* Before using this class, add the following into app.config or web.config

    <system.serviceModel>
      <bindings>
        <basicHttpBinding>
          <binding name="BasicHttpBinding_LanguageService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferSize="65536" maxBufferPoolSize="524288" maxReceivedMessageSize="65536" messageEncoding="Text" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true">
            <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
            <security mode="None">
              <transport clientCredentialType="None" proxyCredentialType="None" realm="" />
              <message clientCredentialType="UserName" algorithmSuite="Default" />
            </security>
          </binding>
        </basicHttpBinding>
      </bindings>
      <client>
        <endpoint address="http://api.microsofttranslator.com/V2/soap.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_LanguageService" contract="TranslatorService.LanguageService" name="BasicHttpBinding_LanguageService" />
      </client>
    </system.serviceModel>

*/

namespace Fuel.QA.Translators
{
    public class MSTranslator : ILanguageDetector
    {
        private Yam.Microsoft.Translator.Translator translator;

        public MSTranslator(string clientId, string clientSecret)
        {
            translator = new Yam.Microsoft.Translator.Translator
                (clientId, clientSecret, string.Empty, string.Empty);
        }

        public string Detect(string str)
        {
            return translator.InvokeWithAuthentication(() => translator.LanguageService.Detect(string.Empty, str));
        }

        public string[] DetectArray(params string[] str)
        {
            return translator.InvokeWithAuthentication(() => translator.LanguageService.DetectArray(string.Empty, str));
        }
    }
}
