﻿using Fuel.QA.Translators;
using NUnit.Framework;

namespace Fuel.QA.Translators.Tests
{
    [TestFixture]
    [Explicit]
    public class SimpleDetectorTests
    {
        private SimpleDetector translator;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            var path = "SDTestData.xml";
            translator = new SimpleDetector(path);
        }

        [TestCase("WB Kids", "it", Result = true)]
        [TestCase("WB Kids", "es", Result = false)]

        [TestCase("LEGS", "en", Result = true)]
        [TestCase("LEGS", "fr", Result = true)]
        [TestCase("LEGS", "it", Result = true)]
        [TestCase("LEGS", "es", Result = false)]
            
        public bool Detect(string str, string lng)
        {
            return translator.Test(str, lng);
        }
    }
}
