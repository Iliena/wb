﻿using Fuel.QA.Translators;
using NUnit.Framework;

namespace Fuel.QA.Translators.Tests
{
    [TestFixture]
    [Explicit]
    public class Yandex
    {
        private YandexTranslator translator;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            translator = new YandexTranslator("trnsl.1.1.20150128T141917Z.d21ba982b7cdef76.9f6bf6310fc306ba7e34d39529f2f3269a5be548");
        }

        [TestCase("tavolo", Result = "it")]
        [TestCase("стол", Result = "ru")]
        [TestCase("Shows", Result = "en")]
        public string Detect(string str)
        {
            return translator.Detect(str);
        }

        [TestCase(1, new string[] { "стол", "Shows" }, Result = new string[] { "ru", "en" })]
        public string[] DetectArray(int a, string[] texts)
        {
            return translator.DetectArray(texts);
        }
    }
}
