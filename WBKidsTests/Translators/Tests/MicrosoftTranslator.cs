﻿using Fuel.QA.Translators;
using NUnit.Framework;

namespace Fuel.QA.Translators.Tests
{
    [TestFixture]
    [Explicit]
    public class Microsofttranslator
    {
        private MSTranslator translator;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            translator = new MSTranslator("abozhko", "27M2rdyhh/prjOq6azPTFpu6iJ2Przzqvef/jymWBK8=");
        }

        [TestCase("tabella", "it")]
        [TestCase("стол", "ru")]
        [TestCase("Shows", "en")]
        public void Detect(string str, string lng)
        {
            var detected = translator.Detect(str);
            Assert.That(detected, Is.EqualTo(lng));
        }

        [TestCase(1, new string[] { "tabella", "стол", "Shows" }, Result = new string[] { "it", "ru", "en" })]
        public string[] DetectArray(int a, string[] texts)
        {
            return translator.DetectArray(texts);
        }
    }
}
