﻿using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Fuel.QA.Selenium.Extensions;
using System.Threading;

namespace Fuel.QA.Core
{
    public delegate void ExceptionHandler(IEnumerable<string> exceptions);

    // public delegate void ExceptionHandler(IEnumerable<ActionScriptException> exceptions);

    public sealed class FlashlogWatcher
    {
        /*
        private static volatile FlashlogWatcher instance;
        private static object syncRoot = new Object();

        public static FlashlogWatcher Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new FlashlogWatcher();
                        }
                    }
                }
                return instance;
            }
        }
        */
        
        static string errorTypes =
            @"error|Error|ArgumentError|AugmentedError|AutomationError|
            CollectionViewError|ComplexCyclicDependencyError|Conflict|
            ConstraintError|CursorError|DataServiceError|DefinitionError|
            DRMManagerError|EvalError|ExpressionError|ExpressionEvaluationError|
            ExpressionValidationError|Fault|FormBridgeError|FunctionSequenceError|
            IllegalOperationError|InvalidCategoryError|InvalidFilterError|InvalidSWFError|
            IOError|ItemPendingError|MediaError|MemoryError|MessagingError|MetricError|
            NoDataAvailableError|PersistenceError|PersistenceError|ProxyServiceError|
            RangeError|ReferenceError|ScriptTimeoutError|SecurityError|
            SimpleCyclicDependencyError|SortError|SQLError|StackOverflowError|
            SyncManagerError|SyntaxError|TypeError|UnresolvedConflictsError|URIError|
            VerifyError|VideoError|VideoError";
        static string ParserBase = "(?:^|\\n)(?:{0}):[\\s\\S]+?(?=\\n[^\\t][^\\n]|$)";
        static string ErrorTypes = Regex.Replace(errorTypes, "\\s", "");
        static Regex Parser = new Regex(string.Format(ParserBase, ErrorTypes), RegexOptions.Compiled);
        
        public event ExceptionHandler ExceptionOccured;

        // public event EventHandler<ASErrorEventHandler> OnException;

        static string LogFileName = "flashlog.txt";
        static string LogFilePath = Environment.ExpandEnvironmentVariables(@"%APPDATA%\Macromedia\Flash Player\Logs\");

        private long LastKnownLogSize;
        private FileSystemWatcher Watcher;

        public FlashlogWatcher()
        {
            if (!Directory.Exists(LogFilePath))
            {
                Console.WriteLine("[{0}] Error: Flashlog path doesn't exist. ActionScript exceptions won't be caught!",
                    GetType().Name);
                return;
            }
            LastKnownLogSize = GetLogSize();

            // setup watcher
            Watcher = new FileSystemWatcher(LogFilePath, "*" + LogFileName);
            Watcher.NotifyFilter = NotifyFilters.LastWrite;
            Watcher.Changed += new FileSystemEventHandler(onLogChange);
            Watcher.EnableRaisingEvents = true;

            Console.WriteLine("[{0}] The listener has been successfully set up", GetType().Name);
        }

        public bool Enabled
        {
            get {
                return Watcher == null ? false : Watcher.EnableRaisingEvents;
            }
            set { 
                if (Watcher != null)
                    Watcher.EnableRaisingEvents = value;
            }
        }

        public static string LogFullPath
        {
            get { return Path.Combine(LogFilePath, LogFileName); }
        }

        public static string GetLogText()
        {
            return TryReadTextFile(LogFullPath);
        }

        public static long GetLogSize()
        {
            FileInfo fileInfo = new FileInfo(LogFullPath);
            return fileInfo.Exists ? fileInfo.Length : 0;
        }

        
        public static IEnumerable<string> ParseExceptions(string logText)
        {
            MatchCollection found = Parser.Matches(logText);
            return found.Cast<Match>().Select(match => match.Groups[0].Value);
        }
        

        private bool scheduled = false;
        private void onLogChange(object src, FileSystemEventArgs e)
        {
            if (scheduled) return;
            // Console.WriteLine(">>> onLogChange (1)");
            scheduled = true;

            var timer = new System.Timers.Timer(250)
            {
                Enabled = true,
                AutoReset = false
            };

            timer.Elapsed += timer_Elapsed;
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // Console.WriteLine("<<< TryReadLogUpdate (2)");
            ((IDisposable)sender).Dispose();
            scheduled = false;
            string LogText;

            if (TryReadLogUpdate(out LogText) && !string.IsNullOrEmpty(LogText))
            {
                var parsed = ParseExceptions(LogText);

                //var parsed = ActionScriptException.ParseMany(LogText);
                if (parsed != null && parsed.Any())
                {
                    //if (this.OnException != null)
                    //{
                    //    this.OnException(this, new ASErrorEventHandler(this, parsed));
                    //}
                    if (ExceptionOccured != null)
                        ExceptionOccured(parsed);
                }
            }
        }

        void FlashlogWatcher_OnException(object sender, ASErrorEventHandler e)
        {
            throw new NotImplementedException();
        }

        private static string TryReadTextFile(string FileName)
        {
            var wait = new DefaultWait<string>(FileName);
            wait.PollingInterval = TimeSpan.FromMilliseconds(50);
            wait.IgnoreExceptionTypes(typeof(IOException));
            return wait.Till(file => File.ReadAllText(file));
        }

        private bool TryReadLogUpdate(out string result)
        {
            var attempts = 20;
            var interval = 500;
            string lastException = string.Empty;
            for (var i = 0; i < attempts; i++)
            {
                try
                {
                    result = ReadLogUpdate();
                    return true;
                }
                catch (Exception e)
                {
                    lastException = e.ToString();
                    // Console.WriteLine("[FlashlogWatcher] {0}", e);
                    System.Threading.Thread.Sleep(interval);
                    continue;
                }
            }
            Console.WriteLine("[{3}] Couldn't read the flashlog after {0} attempts in {1} ms. Last exception thrown:\n{2}",
                attempts, interval, lastException, GetType().Name);
            result = null;
            return false;
        }

        private string ReadLogUpdate()
        {
            using (Stream stream = File.Open(LogFullPath, FileMode.Open, FileAccess.Read/*, FileShare.Read*/))
            {
                string result = string.Empty;
                long currentLength = stream.Length;

                if (currentLength == LastKnownLogSize)
                    return null;

                if (currentLength < LastKnownLogSize)
                    LastKnownLogSize = 0;

                stream.Seek(LastKnownLogSize, SeekOrigin.Begin);
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                LastKnownLogSize = currentLength;
                return result;
            }
        }
    }

    // EvenHandler class
    public class ASErrorEventHandler : EventArgs
    {
        private IEnumerable<ActionScriptException> asExceptions;
        private FlashlogWatcher fWatcher;
        public ASErrorEventHandler(FlashlogWatcher fWatcher, IEnumerable<ActionScriptException> exceptions)
        {
            this.asExceptions = exceptions;
            this.fWatcher = fWatcher;
        }

        public IEnumerable<ActionScriptException> ParsedExceptions { get { return this.asExceptions; } }
        public FlashlogWatcher FWatcher { get { return this.fWatcher; } }
    }
}
