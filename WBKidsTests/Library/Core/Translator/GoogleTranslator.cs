﻿using Fuel.QA.Core.Translator.Correction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Fuel.QA.Core.Translator
{
    public class GoogleTranslator
    {
        string key;
        string languageCode;

        public List<string> cachedText = new List<string>();

        private static GoogleTranslator instance;

        private GoogleTranslator(string key, string languageCode)
        {
            this.key = key;
            this.languageCode = languageCode;
        }
        public static GoogleTranslator GetInstance(string key, string languageCode)
        {
            if (instance == null)
            {
                instance = new GoogleTranslator(key, languageCode);
            }
            return instance;
        }

        public string Detect(string textToDetect)
        {
            if (cachedText.Contains(textToDetect))
                return languageCode;

            var isCorrectCopy = GoogleTranslatorCorrections.DeserializeFromXml().Corrections.Any(c => c.Text.Equals(textToDetect));
            if (isCorrectCopy)
            {
                cachedText.Add(textToDetect);
                return languageCode;
            }

            string uri = String.Format("https://www.googleapis.com/language/translate/v2/detect?key={0}&q={1}", key, textToDetect);
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);

            using (WebResponse response = httpWebRequest.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    String responseString = reader.ReadToEnd();
                    cachedText.Add(textToDetect);
                    return new JavaScriptSerializer().Deserialize<dynamic>(responseString)["data"]["detections"][0][0]["language"];
                }
            }
        }
    }
}
