﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Core.Translator.Correction
{
    public class GoogleTranslatorCorrections
    {
        static string DEFAULT_XML_STORAGE_FOLDER = "HK_TMNT";
        static string xmlFileName = "GoogleTranslatorCorrection.xml";

        public List<DetectCorrection> Corrections { get; set; }

        public static GoogleTranslatorCorrections DeserializeFromXml()
        {
            return Serializer<GoogleTranslatorCorrections>
                .Deserialize(Path.Combine(Utils.GetAssemblyDirectory(), DEFAULT_XML_STORAGE_FOLDER, xmlFileName));
        }
    }
}
