﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fuel.QA.WinAPI;
using System.Diagnostics;
using System.Collections;

namespace Fuel.QA.Core
{
    public class Utils
    {
        static public string GetAssemblyDirectory()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        static public Bitmap CaptureScreen(bool CaptureMouse = false)
        {
            var bounds = Screen.PrimaryScreen.Bounds;
            var bitmap = new Bitmap(bounds.Width, bounds.Height, PixelFormat.Format32bppArgb);
            using (var gfx = Graphics.FromImage(bitmap))
            {
                gfx.CopyFromScreen(bounds.X, bounds.Y, 0, 0, bounds.Size, CopyPixelOperation.SourceCopy);

                // Add cursor
                if (CaptureMouse)
                {
                    User32.CURSORINFO pci;
                    pci.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(User32.CURSORINFO));

                    if (User32.GetCursorInfo(out pci))
                    {
                        if (pci.flags == User32.CURSOR_SHOWING)
                        {
                            User32.DrawIcon(gfx.GetHdc(), pci.ptScreenPos.x, pci.ptScreenPos.y, pci.hCursor);
                            gfx.ReleaseHdc();
                        }
                    }
                }
            }
            return bitmap;
        }

        static public void CaptureScreenshot(string file, bool CaptureMouse = false)
        {
            Bitmap screenshot = CaptureScreen(CaptureMouse);
            screenshot.Save(file);
        }

        static public string RemoveInvalidPathChars(string illegal)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(illegal, "");
        }

        static public void KillProcessNamed(params string[] names)
        {
            var processes = new List<Process>();
            foreach (var name in names)
            {
                try
                {
                    processes.AddRange(Process.GetProcessesByName(name));
                }
                catch (InvalidOperationException) { }
            }

            foreach (var proc in processes)
            {
                try
                {
                    proc.Kill();
                    Console.WriteLine("Killed process: [{0}] {1}", proc.Id, proc.ProcessName);
                }
                catch (Exception) { }
            }
        }


        /// <summary>
        /// Attempts to open a readable stream for the data downloaded from a resource
        /// with the URI specified as a System.String
        /// </summary>
        /// <param name="address">The URI specified as a System.String from which to download data.</param>
        /// <returns>A value indicating whether the stream was opened successfully</returns>
        public static bool IsInternetConnected(string address = "http://www.google.com")
        {
            try
            {
                using (var client = new System.Net.WebClient())
                using (var stream = client.OpenRead(address))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        private static Random random = new Random();
        public static string GetRandomAlphanumericString(int length)
        {
            var numbers = Enumerable.Range('0', '9' - '0' + 1);
            var lowercase = Enumerable.Range('a', 'z' - 'a' + 1);
            var uppercase = Enumerable.Range('A', 'Z' - 'A' + 1);
            var chars = numbers.Concat(lowercase).Concat(uppercase).Select(c => (char)c).ToArray();

            var result = new char[length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = chars[random.Next(chars.Length)];
            }
            return new String(result);
        }
    }
}
