﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace Fuel.QA.Core
{
    // TODO: Write usage/installation instructions
    public class VideoRecorder
    {
        static string ffmpeg_executable = "ffmpeg.exe";

        /// <summary>
        /// -f fmt              force format
        /// -vcodec codec       force video codec ('copy' to copy stream)
        /// -r rate             set frame rate (Hz value, fraction or abbreviation)
        /// -y                  overwrite output files
        /// </summary>
        static string ffmpeg_arguments = "-loglevel fatal -f dshow -i video=screen-capture-recorder -vcodec libx264 -crf 35 -r 24000/1001 -q 1 {0} -y";

        public String VideoFileName { get; set; }

        private Process Process { get; set; }


        //TODO: test
        static VideoRecorder()
        {
            Console.WriteLine("static VideoRecorder()");
            Utils.KillProcessNamed(ffmpeg_executable);
        }

        public VideoRecorder(string videoFileName)
        {
            VideoFileName = videoFileName;
        }

        /// <summary>
        /// Starts video recording
        /// </summary>
        /// <returns></returns>
        public VideoRecorder Start()
        {
            var procStartInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                FileName = ffmpeg_executable,
                Arguments = String.Format(ffmpeg_arguments, VideoFileName),
                RedirectStandardOutput = true,
                RedirectStandardInput = true
            };

            try
            {
                Process = Process.Start(procStartInfo);
            }
            catch (Win32Exception e)
            {
                throw new VideoRecorderException("Apparently, " + ffmpeg_executable + " executable not found", e);
            }
            return this;
        }

        /// <summary>
        /// Stops video recording
        /// </summary>
        public void Stop()
        {
            if (Process == null)
            {
                throw new VideoRecorderException("Can't stop recording because it hasn't been started. Please call Start() first.");
            }
            Process.StandardInput.Write("q");
        }
    }

    public class VideoRecorderException : Exception
    {
        public VideoRecorderException(string message)
            : base(message) { }
        public VideoRecorderException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
