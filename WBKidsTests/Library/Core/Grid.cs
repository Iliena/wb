﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace Fuel.QA.Core
{
    public class Grid
    {
        Rectangle rect;
        int hNum;
        int vNum;

        public Grid(Rectangle rect, int hNum, int vNum)
        {
            this.rect = rect;
            this.hNum = hNum;
            this.vNum = vNum;
        }

        public Grid(int x, int y, int width, int height, int hNum, int vNum)
            : this(new Rectangle(x, y, width, height), hNum, vNum) { }

        public Grid(Rectangle rect, Size size)
            : this(rect, size.Width, size.Height) { }

        public Point Get(int index)
        {
            index %= this.vNum * this.hNum;
            double i = index % hNum;
            double j = Math.Floor((double)index / hNum);

            double w = hNum > 1 ? rect.Width / (hNum - 1) : 0;
            double h = vNum > 1 ? rect.Height / (vNum - 1) : 0;

            int X = Convert.ToInt32(rect.Left + w * i);
            int Y = Convert.ToInt32(rect.Top + h * j);

            return new Point(X, Y);
        }
    }
}
