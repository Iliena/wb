﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

using Fuelyouth.WinAPI;

namespace Fuelyouth.QA
{
    class FlashDriver : WindowDriver
    {
        private const String CHROME_WND_CLASS = "NativeWindowClass";
        private const String FIREFOX_WND_CLASS = "GeckoPluginWindow";
        private const String IEXPLORE_WND_CLASS = "MacromediaFlashPlayerActiveX";

        public FlashDriver(IWebDriver driver) : base(driver)
        {
            // Nothing to do, just call the base constructor
        }

        protected override IntPtr GetWindow()
        {
            switch (((RemoteWebDriver)driver).Capabilities.BrowserName)
            {
                case "chrome":
                    return User32Helper.FindWindowByClassName(CHROME_WND_CLASS);
                case "firefox":
                    return User32Helper.FindWindowByClassName(FIREFOX_WND_CLASS);
                case "internet explorer":
                    return User32Helper.FindWindowByClassName(IEXPLORE_WND_CLASS);
                default:
                    throw new Exception("Browser not supported");
            }
        }

        protected override void SetForeground()
        {
            IntPtr parent;
            switch (((RemoteWebDriver)driver).Capabilities.BrowserName)
            {
                case "chrome":
                case "firefox":
                    parent = User32Helper.GetParent(GetWindow(), 1);
                    break;
                case "internet explorer":
                    parent = User32Helper.GetParent(GetWindow(), 4);
                    break;
                default:
                    throw new Exception("Browser not supported");
            }
            User32.SetForegroundWindow(parent);
        }

        protected override bool isValidHandle(IntPtr hWnd)
        {
            if(hWnd == IntPtr.Zero || !User32.IsWindow(hWnd))
                return false;

            string className;
            switch (((RemoteWebDriver)driver).Capabilities.BrowserName)
            {
                case "chrome":
                    className = CHROME_WND_CLASS;
                    break;
                case "firefox":
                    className = FIREFOX_WND_CLASS;
                    break;
                case "internet explorer":
                    className = IEXPLORE_WND_CLASS;
                    break;
                default:
                    throw new Exception("Browser not supported");
            }
            return className.CompareTo(User32Helper.GetWindowClass(hWnd)) == 0;
        }
    }
}
