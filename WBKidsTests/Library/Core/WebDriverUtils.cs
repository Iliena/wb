﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.IO;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Fuelyouth.QA
{
    class WebDriverUtils
    {
        static public IWebDriver InitDriver(ConsoleKey key)
        {
            Console.WriteLine("Initializing driver... ");
            switch (key)
            {
                case ConsoleKey.C:
                    ChromeOptions chromeOptions = new ChromeOptions();
                    //chromeOptions.AddArgument("--kiosk");
                    //chromeOptions.AddArgument(@"--window-size=1125,900");
                    //chromeOptions.AddArgument(@"--disable-bundled-ppapi-flash");

                    chromeOptions.AddArgument(@"--always-authorize-plugins");
                    //chromeOptions.AddArgument(@"--enable-browser-plugin-for-all-view-types");
                    return new ChromeDriver(chromeOptions);
                case ConsoleKey.I:
                    InternetExplorerOptions ieOptions = new InternetExplorerOptions();
                    ieOptions.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    ieOptions.InitialBrowserUrl = "about:blank";
                    //ieOptions.EnableNativeEvents = false;

                    //Kiosk
                    //ieOptions.ForceCreateProcessApi = true;
                    //ieOptions.BrowserCommandLineArguments = "";
                    return new InternetExplorerDriver(ieOptions);
                case ConsoleKey.F:
                    IWebDriver d = new FirefoxDriver();
                    //((FirefoxDriver)d).Keyboard.PressKey(Keys.F11);
                    //d.FindElement(By.TagName("html")).SendKeys(Keys.F11);
                    return d;
            }
            return null;
        }

        public static string[] GetPageStrings(IWebDriver driver)
        {
            By[] locators = new By[]
            {
                By.TagName("p"),
                By.TagName("a"),
                By.TagName("span"),
                By.TagName("font"),
                By.TagName("label"),
                By.TagName("h1"),
                By.TagName("h2"),
                By.TagName("h3"),
                By.TagName("h4"),
                By.TagName("h5"),
                By.TagName("input")
            };

            //List<IWebElement> elements = new List<IWebElement>();
            IEnumerable<IWebElement> elements = Enumerable.Empty<IWebElement>();
            foreach (By locator in locators)
            {
                IReadOnlyCollection<IWebElement> els = driver.FindElements(locator);
                elements = elements.Concat(els);
            }

            IEnumerable<string> strs = elements.Select(x => { try { return x.Text; } catch (Exception) { } return ""; });
            strs = strs.Where(s => s != "");
            return strs.ToArray();


            //string[] textNodes = new string[]
            //{
            //    "//p",
            //    "//a",
            //    "//span",
            //    "//font",
            //    "//label",
            //    "//h1",
            //    "//h2",
            //    "//h3",
            //    "//h4",
            //    "//h5",
            //    "//input/@placeholder"
            //};
            
            //string xpath = String.Join("|", textNodes);
            //IReadOnlyCollection<IWebElement> elems = driver.FindElements(By.XPath(xpath));

            //List<string> strings = new List<string>();
            //foreach (IWebElement elem in elems)
            //{
            //    try
            //    {
            //        string text = elem.Text;
            //        if (text!=null && text != "")
            //            strings.Add(elem.Text);
            //    }
            //    catch (Exception e) { }
            //}
            //return strings.ToArray();
        }

        public static string GetPageText(IWebDriver driver)
        {
            return driver.FindElement(By.TagName("body")).Text;
        }

        static Bitmap ShootWebElement(IWebElement elem, IWebDriver driver)
        {
            Screenshot screen = ((ITakesScreenshot)driver).GetScreenshot();
            Bitmap bm = new Bitmap(new MemoryStream(screen.AsByteArray));
            Rectangle crop = new Rectangle(elem.Location.X, elem.Location.Y, Math.Min(elem.Size.Width, bm.Width), Math.Min(elem.Size.Height, bm.Height));
            return bm.Clone(crop, bm.PixelFormat);
        }

        public static void WaitForAnimationIsDone(IWebDriver driver, By locator)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement element = wait.Until(ExpectedConditions.ElementIsVisible(locator));
            WaitForAnimationIsDone(driver, element);
        }

        public static void WaitForAnimationIsDone(IWebDriver driver, IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(new SystemClock(), driver, TimeSpan.FromSeconds(10), TimeSpan.FromMilliseconds(50));
            Point location = new Point();
            int hit = 0;
            wait.Until(d =>
            {
                hit = element.Location.Equals(location) ? hit + 1 : 0;
                location = element.Location;
                Console.Out.WriteLine(location);
                return hit > 5;
            });
        }

        public static void WaitElementThenClick(IWebDriver driver, By locator)
        {
            
            IWebElement element = new WebDriverWait(driver, TimeSpan.FromSeconds(100)).Until(ExpectedConditions.ElementIsVisible(locator));

            new Actions(driver).MoveToElement(element, element.Size.Width / 2, element.Size.Height / 2).Click().Perform();
        }

        public static void WaitForPageLoad(IWebDriver _driver, int maxWaitTimeInSeconds)
        {
            string state = string.Empty;
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(maxWaitTimeInSeconds));

                //Checks every 500 ms whether predicate returns true if returns exit otherwise keep trying till it returns ture
                wait.Until(d =>
                {
                    try
                    {
                        state = ((IJavaScriptExecutor)_driver).ExecuteScript(@"return document.readyState").ToString();
                    }
                    catch (InvalidOperationException)
                    {
                        //Ignore
                    }
                    catch (NoSuchWindowException)
                    {
                        //when popup is closed, switch to last windows
                        //_driver.SwitchTo().Window(_driver.WindowHandles.Last());
                    }
                    //In IE7 there are chances we may get state as loaded instead of complete
                    return (state.Equals("complete", StringComparison.InvariantCultureIgnoreCase) || state.Equals("loaded", StringComparison.InvariantCultureIgnoreCase));

                });
            }
            catch (TimeoutException)
            {
                //sometimes Page remains in Interactive mode and never becomes Complete, then we can still try to access the controls
                if (!state.Equals("interactive", StringComparison.InvariantCultureIgnoreCase))
                    throw;
            }
            catch (NullReferenceException)
            {
                //sometimes Page remains in Interactive mode and never becomes Complete, then we can still try to access the controls
                if (!state.Equals("interactive", StringComparison.InvariantCultureIgnoreCase))
                    throw;
            }
            catch (WebDriverException)
            {
                if (_driver.WindowHandles.Count == 1)
                {
                    _driver.SwitchTo().Window(_driver.WindowHandles[0]);
                }
                state = ((IJavaScriptExecutor)_driver).ExecuteScript(@"return document.readyState").ToString();
                if (!(state.Equals("complete", StringComparison.InvariantCultureIgnoreCase) || state.Equals("loaded", StringComparison.InvariantCultureIgnoreCase)))
                    throw;
            }
        }
    }
}
