﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Fuel.QA.Core
{
    public class CompareBitmap
    {
        private Bitmap bitmap;

        public CompareBitmap(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }

        public bool WithPattern(Bitmap pattern, int colorTolerance = 0)
        {
            return CompareBitmap.WithPattern(bitmap, pattern, colorTolerance);
        }

        public bool WithSubPicture(Bitmap pattern, int colorTolerance = 0)
        {
            return CompareBitmap.WithSubPicture(bitmap, pattern, colorTolerance);
        }

        public bool WithFile(String file, int colorTolerance = 0)
        {
            return CompareBitmap.WithFile(bitmap, file, colorTolerance);
        }

        static public Bitmap ResizeCanvas(Bitmap bitmap, Size size)
        {
            Bitmap resized = new Bitmap(size.Width, size.Height);
            Graphics gfx = Graphics.FromImage(resized);
            gfx.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
            return resized;
        }

        static public Boolean WithFile(Bitmap bmp, String file, int colorTolerance = 0)
        {
            Bitmap pattern = new Bitmap(file);
            return WithPattern(bmp, pattern, colorTolerance);
        }


        static public unsafe Boolean WithPattern(Bitmap bitmap, Bitmap pattern, int colorTolerance = 0)
        {
            bitmap = ResizeCanvas(bitmap, pattern.Size);

            // Start the magic
            BitmapData bData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            BitmapData pData = pattern.LockBits(new Rectangle(0, 0, pattern.Width, pattern.Height), ImageLockMode.ReadOnly, pattern.PixelFormat);

            int bPixFmt = Image.GetPixelFormatSize(bitmap.PixelFormat);
            int pPixFmt = Image.GetPixelFormatSize(pattern.PixelFormat);

            byte* bScan0 = (byte*)bData.Scan0.ToPointer();
            byte* pScan0 = (byte*)pData.Scan0.ToPointer();

            for (var y = 0; y < pData.Height; ++y)
            {
                for (var x = 0; x < pData.Width; ++x)
                {
                    byte* pPixel = pScan0 + y * pData.Stride + x * pPixFmt / 8;
                    byte* bPixel = bScan0 + y * bData.Stride + x * bPixFmt / 8;

                    // Black pixel in pattern image
                    if (pPixel[0] == 0 && pPixel[0] == pPixel[1] && pPixel[1] == pPixel[2])
                        continue;

                    if (x >= bitmap.Width || y >= bitmap.Height ||
                        Math.Abs(bPixel[0] - pPixel[0]) > colorTolerance ||
                        Math.Abs(bPixel[1] - pPixel[1]) > colorTolerance ||
                        Math.Abs(bPixel[2] - pPixel[2]) > colorTolerance
                        )
                    {
                        // A different pixel found
                        bitmap.UnlockBits(bData);
                        pattern.UnlockBits(pData);
                        return false;
                    }
                }
            }

            // Bitmaps matches the pattern
            bitmap.UnlockBits(bData);
            pattern.UnlockBits(pData);
            return true;
        }

        static public unsafe Boolean WithSubPicture(Bitmap main, Bitmap sub, int colorTolerance = 0)
        {
            List<Rectangle> acceptedPos = new List<Rectangle>();

            int mainwidth = main.Width;
            int mainheight = main.Height;

            int subwidth = sub.Width;
            int subheight = sub.Height;

            int movewidth = mainwidth - subwidth;
            int moveheight = mainheight - subheight;

            BitmapData bmMainData = main.LockBits(new Rectangle(0, 0, mainwidth, mainheight), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData bmSubData = sub.LockBits(new Rectangle(0, 0, subwidth, subheight), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            int bytesMain = Math.Abs(bmMainData.Stride) * mainheight;
            int strideMain = bmMainData.Stride;
            System.IntPtr Scan0Main = bmMainData.Scan0;
            byte[] dataMain = new byte[bytesMain];
            System.Runtime.InteropServices.Marshal.Copy(Scan0Main, dataMain, 0, bytesMain);

            int bytesSub = Math.Abs(bmSubData.Stride) * subheight;
            int strideSub = bmSubData.Stride;
            System.IntPtr Scan0Sub = bmSubData.Scan0;
            byte[] dataSub = new byte[bytesSub];
            System.Runtime.InteropServices.Marshal.Copy(Scan0Sub, dataSub, 0, bytesSub);

            int firstNotBlackPositionX = 0;
            int firstNotBlackPositionY = 0;

            for (int y = 0; y < subheight; ++y)
            {
                bool notBlackFounded = false;
                for (int x = 0; x < subwidth; ++x)
                {
                    PixelColor col = GetColor(x, y, strideSub, dataSub);
                    if (!col.IsBlack()) 
                    {
                        firstNotBlackPositionX = x;
                        firstNotBlackPositionY = y;
                        notBlackFounded = true;
                        break;
                    }                   
                }
                if (notBlackFounded)
                    break;
            }

            for (int y = 0; y < moveheight; ++y)
            {
                for (int x = 0; x < movewidth; ++x)
                {
                    PixelColor curcolor = GetColor(x, y, strideMain, dataMain);

                    if (curcolor.Like(GetColor(firstNotBlackPositionX, firstNotBlackPositionY, strideSub, dataSub), colorTolerance))
                    {
                        var possibleRectangle = new Rectangle(x, y, subwidth, subheight);

                        int xsub = x - possibleRectangle.X;
                        int ysub = y - possibleRectangle.Y;
                        if (xsub >= subwidth || ysub >= subheight || xsub < 0)
                            continue;

                        bool isSubPicture = true;
                        for (int j = firstNotBlackPositionX; j < subheight; j++)
                        {
                            for (int i = firstNotBlackPositionY; i < subwidth; i++)
                            {

                                PixelColor subcolor1 = GetColor(i, j, strideSub, dataSub);

                                if (subcolor1.IsBlack())
                                    continue;

                                PixelColor color1 = GetColor(possibleRectangle.X + i - firstNotBlackPositionX, possibleRectangle.Y + j - firstNotBlackPositionX, strideMain, dataMain);

                                if (!color1.Like(subcolor1, colorTolerance))
                                {
                                    isSubPicture = false;
                                    break;
                                }
                            }
                            if (!isSubPicture)
                                break;
                        }
                        if(isSubPicture)
                            acceptedPos.Add(possibleRectangle);
                    }

                }
            }

            System.Runtime.InteropServices.Marshal.Copy(dataSub, 0, Scan0Sub, bytesSub);
            sub.UnlockBits(bmSubData);

            System.Runtime.InteropServices.Marshal.Copy(dataMain, 0, Scan0Main, bytesMain);
            main.UnlockBits(bmMainData); ;

            return acceptedPos.Count > 0;
        }

        public List<Rectangle> GetPossibleRectangles()
        {
            List<Rectangle> rectangles = new List<Rectangle>();
            return rectangles;
        }


        private static PixelColor GetColor(Point point, int stride, byte[] data)
        {
            return GetColor(point.X, point.Y, stride, data);
        }

        private static PixelColor GetColor(int x, int y, int stride, byte[] data)
        {
            int pos = y * stride + x * 4;
            byte r = data[pos + 2];
            byte g = data[pos + 1];
            byte b = data[pos + 0];
            return PixelColor.FromRGB(r, g, b);
        }

        struct PixelColor
        {
            byte R;
            byte G;
            byte B;

            public static PixelColor FromRGB(byte r, byte g, byte b)
            {
                PixelColor pixelColor = new PixelColor();
                pixelColor.R = r;
                pixelColor.G = g;
                pixelColor.B = b;
                return pixelColor;
            }

            public bool IsBlack()
            {
                return R == 0 && G == 0 && B == 0;
            }


            public bool Like(object obj, int colorTolerance)
            {
                if (!(obj is PixelColor))
                    return false;
                PixelColor color = (PixelColor)obj;

                if (
                     Math.Abs(color.R - this.R) > colorTolerance ||
                     Math.Abs(color.G - this.G) > colorTolerance ||
                     Math.Abs(color.B - this.B) > colorTolerance
                     )
                    return false;
                else
                    return true;
            }

        }

    }
}