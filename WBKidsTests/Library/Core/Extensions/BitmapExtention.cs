﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Core.Extensions
{
    public static class BitmapExtention
    {
        public static Bitmap ToMonochrome(this Bitmap bmpImg, int P)
        {
            Bitmap result = new Bitmap(bmpImg.Width, bmpImg.Height);
            Color color = new Color();
            try
            {
                for (int j = 0; j < bmpImg.Height; j++)
                {
                    for (int i = 0; i < bmpImg.Width; i++)
                    {
                        color = bmpImg.GetPixel(i, j);
                        int K = (color.R + color.G + color.B) / 3;
                        result.SetPixel(i, j, (K <= P ? Color.Black : Color.White));
                    }
                }
            }
            catch
            {
                throw new InvalidOperationException("Failed to convert the image to monochrome");
            }
            return result;
        }
    }
}
