﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;

namespace Fuel.QA.Core
{
    public class PatternCollection
    {
        static String DEFAULT_PATTERNS_DIR = "Patterns";

        public String PatternsDirectory
        {
            get
            {
                return Path.Combine(Utils.GetAssemblyDirectory(), DEFAULT_PATTERNS_DIR);
            }
        }
        public PatternCollection()
        {

        }

        public IEnumerable<PatternFile> GetPatterns(string id)
        {
            String[] FileNames = Directory.GetFiles(PatternsDirectory);
            var patterns = FileNames.Select(f => new PatternFile(f)).Where(p => p.Id == id);

            if (!patterns.Any())
            {
                throw new ArgumentException("No pattern files found for id '" + id + "'", "id");
            }

            return patterns;
        }
    }

    public class Pattern
    {
        static string DEFAULT_DIRECTORY = "Patterns";
        public string Id { get; set; }

        public Pattern(string id)
            : this(id, GetDefaultPath()) { }

        public Pattern(string id, string path)
        {
            this.Id = id;
            this.path = path;

            if (PatternFiles.Count == 0)
            {
                ThrowNotFoundException();
            }
        }

        public bool MatchesWith(Bitmap bitmap)
        {
            return PatternFiles.Any(p => new CompareBitmap(bitmap).WithPattern(p.Bitmap, p.Tolerance));
        }

        public bool IsPartOf(Bitmap bitmap)
        {
            return PatternFiles.Any(p => new CompareBitmap(bitmap).WithSubPicture(p.Bitmap, p.Tolerance));
        }

        private List<PatternFile> PatternFiles
        {
            get
            {
                return Directory.GetFiles(path)
                    .Select(fileName => new PatternFile(fileName))
                    .Where(p => p.Id == this.Id)
                    .ToList();
            }
        }

        private void ThrowNotFoundException()
        {
            string message = string.Format("No pattern files found for id {0}", Id);
            string fileName = Path.Combine(path, Id + "*.png");

            throw new FileNotFoundException(message, fileName);
        }

        private string path;
        private static string GetDefaultPath()
        {
            return Path.Combine(Utils.GetAssemblyDirectory(), DEFAULT_DIRECTORY);
        }
    }

    public class PatternFile
    {
        public PatternFile(string name)
        {
            this.Name = name;
            this.lazyBitmap = new Lazy<Bitmap>(() => new Bitmap(Name));
        }

        public string Id { get; set; }
        public string Version { get; set; }
        public int Tolerance { get; set; }
        public Bitmap Bitmap { get { return lazyBitmap.Value; } }

        /// <summary>
        /// Pattern file name
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            private set
            {
                _name = value;

                string basestr = Path.GetFileNameWithoutExtension(value);

                Match match = idParser.Match(basestr);
                this.Id = match.Groups["Id"].ToString();

                string meta = match.Groups["Meta"].ToString();

                string toleranceStr = toleranceParser.Match(meta).Groups["Tolerance"].ToString();
                this.Tolerance = string.IsNullOrEmpty(toleranceStr) ? 0 : Int32.Parse(toleranceStr);

                this.Version = versionParser.Match(meta).Groups["Version"].ToString();
            }
        }

        private Lazy<Bitmap> lazyBitmap;
        private string _name;

        static private Regex idParser = new Regex("^(?<Id>[^-]+)(?<Meta>.*)$");
        static private Regex toleranceParser = new Regex("-c(?<Tolerance>[^-]+)(?:-|$)");
        static private Regex versionParser = new Regex("-v(?<Version>[^-]+)(?:-|$)");
    }
}
