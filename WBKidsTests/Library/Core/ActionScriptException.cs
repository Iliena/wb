﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Fuel.QA.Core
{
    public class ActionScriptException
    {
        static string errorTypes =
            @"error|Error|ArgumentError|AugmentedError|AutomationError|
            CollectionViewError|ComplexCyclicDependencyError|Conflict|
            ConstraintError|CursorError|DataServiceError|DefinitionError|
            DRMManagerError|EvalError|ExpressionError|ExpressionEvaluationError|
            ExpressionValidationError|Fault|FormBridgeError|FunctionSequenceError|
            IllegalOperationError|InvalidCategoryError|InvalidFilterError|InvalidSWFError|
            IOError|ItemPendingError|MediaError|MemoryError|MessagingError|MetricError|
            NoDataAvailableError|PersistenceError|PersistenceError|ProxyServiceError|
            RangeError|ReferenceError|ScriptTimeoutError|SecurityError|
            SimpleCyclicDependencyError|SortError|SQLError|StackOverflowError|
            SyncManagerError|SyntaxError|TypeError|UnresolvedConflictsError|URIError|
            VerifyError|VideoError|VideoError";
        static string ErrorTypes = Regex.Replace(errorTypes, "\\s", "");

        static string ParserBase = "(?:^|\\n)(?:{0}):[\\s\\S]+?(?=\\n[^\\t][^\\n]|$)";
        static Regex Parser = new Regex(string.Format(ParserBase, ErrorTypes), RegexOptions.Compiled);

        public static IEnumerable<string> ParseExceptions(string source)
        {
            MatchCollection found = Parser.Matches(source);
            return found.Cast<Match>().Select(match => match.Groups[0].Value);
        }

        public static IEnumerable<ActionScriptException> ParseMany(string text)
        {
            MatchCollection found = Parser.Matches(text);
            foreach (Match matchItem in found)
            {
                yield return new ActionScriptException(matchItem.Groups[0].Value);
            }
            // return found.Cast<Match>().Select(match => new ActionScriptException(match.Groups[0].Value));
        }

        public string Source { get; set; }
        protected ActionScriptException(string source)
        {
            this.Source = source;
        }

        public void WriteTo(System.IO.TextWriter writer)
        {
            string output = string.Format(" \n{0} Action Script Error ({1} chars, {2} line/s) {0}\n{3}\n{4}\n ",
                new String('~', 14), Source.Length, Source.Split('\n').Length, Source, new String('~', 71));

            writer.WriteLine(output);
        }
    }
}
