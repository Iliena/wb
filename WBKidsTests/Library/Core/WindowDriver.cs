﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Imaging;

using System.Windows.Forms;

using Fuelyouth.WinAPI;

using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

using WindowsInput;
using WindowsInput.Native;

namespace Fuelyouth.QA
{
    abstract class WindowDriver
    {
        #region Abstract methods
        protected abstract IntPtr GetWindow();
        protected abstract bool isValidHandle(IntPtr hWnd);
        protected abstract void SetForeground();
        #endregion

        protected IntPtr _hWnd;
        protected IWebDriver driver;

        public IWebDriver Driver { get {return driver;} }

        protected IntPtr hWnd { get {
            if (!isValidHandle(_hWnd))
            {
                System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
                timer.Start();

                _hWnd = GetWindow();
                timer.Stop();

                Console.WriteLine(String.Format("[WindowDriver] hWnd set to: 0x{0} [{1}]. Operation took {2}ms", _hWnd.ToString("X8"), User32Helper.GetWindowClass(_hWnd), timer.ElapsedMilliseconds));
            }
            return _hWnd;
        }}

        public WindowDriver(IWebDriver driver)
        {
            this.driver = driver;
        }

        public bool Ready { get { return hWnd != IntPtr.Zero; } }

        public Bitmap Capture(){
            SetForeground();

            RECT rect = this.Rect;
            Bitmap bmp = new Bitmap(rect.Width, rect.Height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.Left, rect.Top, 0, 0, rect.Size, CopyPixelOperation.SourceCopy);

            return bmp;
        }


        public RECT Rect
        {
            get
            {
                RECT rect = new RECT();
                User32.GetWindowRect(hWnd, ref rect);
                return rect;
            }
        }

        public void Click(int x, int y)
        {
            Click(new Point(x, y));
        }

        public void Click(Point point)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            Hover(point);
            new InputSimulator().Mouse.LeftButtonClick();

            timer.Stop();
            Console.Out.WriteLine("WinAPI / Click took " + timer.ElapsedMilliseconds + "ms");
        }

        public void Hover(int x, int y)
        {
            this.Hover(new Point(x, y));
        }

        public void Hover(Point point)
        {
            SetForeground();
            Cursor.Position = GetAbsoluteCoords(point);
        }

        private Point GetAbsoluteCoords(Point RelativeCoords)
        {
            RECT rect = this.Rect;
            int X = rect.Location.X + RelativeCoords.X;
            int Y = rect.Location.Y + RelativeCoords.Y;
            return new Point(X, Y);
        }

        public void Wait(String id, int tolerance, TimeSpan timeout)
        {
            Screen p = new Screen(id);
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.Until(d => p.Match(Capture(), tolerance));
        }

        public void Wait(String id)
        {
            Wait(id, 20, TimeSpan.FromSeconds(100));
        }

        public bool Match(String id, int tolerance)
        {
            return new Screen(id).Match(Capture(), tolerance);
        }

        /// <summary>
        /// Simulates text entry
        /// </summary>
        /// <param name="text">The text to be simulated</param>
        public void SendKeys(String text)
        {
            SetForeground();
            new InputSimulator().Keyboard.TextEntry(text);
        }

        /// <summary>
        /// Simulates the key press
        /// </summary>
        /// <param name="keyCode"></param>
        public void KeyPress(VirtualKeyCode keyCode)
        {
            SetForeground();
            new InputSimulator().Keyboard.KeyPress(keyCode);
        }
    }
}
