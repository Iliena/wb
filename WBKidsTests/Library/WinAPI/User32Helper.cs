﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.WinAPI
{
    class User32Helper
    {
        public static string GetWindowClass(IntPtr hWnd)
        {
            // Pre-allocate 256 characters, since this is the maximum class name length.
            StringBuilder className = new StringBuilder(256);
            int nRet = User32.GetClassName(hWnd, className, className.Capacity);
            if (nRet != 0)
            {
                return className.ToString();
            }
            else return null;
        }
        public static IntPtr FindWindowByClassName(string className, IntPtr hWnd = default(IntPtr))
        {
            IEnumerable<IntPtr> windows = GetChildWindows(hWnd);
            foreach (IntPtr wnd in windows)
            {
                string wndClass = GetWindowClass(wnd);
                if (wndClass != null && wndClass.CompareTo(className) == 0 && User32.IsWindowVisible(wnd))
                {
                    // found
                    return wnd;
                }

                IntPtr w = FindWindowByClassName(className, wnd);
                if (w != IntPtr.Zero)
                    return w;
            }
            return IntPtr.Zero;
        }
        private static IEnumerable<IntPtr> GetChildWindows(IntPtr hWnd = default(IntPtr))
        {
            List<IntPtr> windows = new List<IntPtr>();
            User32.EnumChildWindows(hWnd, delegate(IntPtr wnd, IntPtr param)
            {
                if (param.ToString().CompareTo(wnd.ToString()) != 0)
                    windows.Add(wnd);
                return true;
            }, IntPtr.Zero);
            return windows;
        }
        public static IntPtr GetParent(IntPtr hWnd, uint depth = 0)
        {
            IntPtr parent = hWnd;
            do
            {
                parent = User32.GetParent(parent);
            } while (depth-- > 0 && User32.IsWindow(parent));
            return parent;
        }
        public static Rectangle GetWindowRectangle(IntPtr hWnd)
        {
            RECT rect = new RECT();
            User32.GetWindowRect(hWnd, ref rect);
            return new Rectangle(rect.Location, rect.Size);
        }
    }
}
