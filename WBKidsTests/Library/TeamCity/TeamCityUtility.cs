﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.TeamCity
{
    public class TeamCityUtility
    {
        public static bool IsOnTeamCity()
        {
            return Environment.GetEnvironmentVariable("TEAMCITY_VERSION") != null;
        }
    }
}
