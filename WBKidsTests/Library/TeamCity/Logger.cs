﻿using Fuel.QA.Core;
using JetBrains.TeamCity.ServiceMessages.Write.Special;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Fuel.QA.TeamCity
{
    public class Logger
    {
        static string LogSubfolder = "FuelTestLogs";

        #region Static Methods
        public static void Message(string message)
        {
            //new TeamCityServiceMessages().CreateWriter().WriteMessage(message);
            Console.WriteLine(message);
        }

        public static void Message(string message, params object[] args)
        {
            Message(String.Format(message, args));
        }

        public static void Error(string message)
        {
            new TeamCityServiceMessages().CreateWriter().WriteError(message);
        }
        public static void PublishFile(string fileName)
        {
            // Logger.Message("Publishing file " + fileName);
            new TeamCityServiceMessages().CreateWriter().PublishArtifact(fileName);
        }
        public static void CleanUp()
        {
            var tempDir = System.IO.Path.Combine(System.IO.Path.GetTempPath(), LogSubfolder);
            var dir = new System.IO.DirectoryInfo(tempDir);
            if (dir.Exists)
                dir.Delete(true);
        }
        public static string GetTempFilePath(string name, string extension = null)
        {
            string fileName = Utils.RemoveInvalidPathChars(name);
            fileName = fileName.Replace(" ", "_").Replace(",", "_");

            if (extension != null)
            {
                fileName += "." + Utils.RemoveInvalidPathChars(extension);
            }
            return System.IO.Path.Combine(GetDirectory(), fileName);
        }
        private static string GetDirectory()
        {
            var directory = Path.Combine(Path.GetTempPath(), LogSubfolder, DateTime.Now.Ticks.ToString());
            Directory.CreateDirectory(directory);
            return directory;
        }
        #endregion

        Lazy<HashSet<string>> lazyQueue;
        protected HashSet<string> Queue { get { return lazyQueue.Value; } }

        Lazy<List<Tuple<string, string>>> lazyStatQueue;
        protected List<Tuple<string, string>> StatQueue { get { return lazyStatQueue.Value; } }


        ITeamCityWriter writer;
        public Logger()
        {
            lazyQueue = new Lazy<HashSet<string>>(() => new HashSet<string>());
            lazyStatQueue = new Lazy<List<Tuple<string, string>>>(() => new List<Tuple<string, string>>());

            writer = new TeamCityServiceMessages().CreateWriter();
        }

        public void Error(string message, string details = null)
        {
            // writer.WriteError(message, details);

            var msg = details == null ? message : message + "\n\n" + details;
            writer.WriteWarning(details);
        }

        public void ScheduleFilePublishing(string fileName)
        {
            Queue.Add(fileName);
        }

        public void SchedulePublishing(Bitmap bitmap, string name)
        {
            string fileName = GetTempFilePath(name, "png");
            Logger.Message("Saving bitmap to '{0}'", fileName);
            bitmap.Save(fileName);
            ScheduleFilePublishing(fileName);
        }

        public void SchedulePublishing(string contents, string name)
        {
            var fileName = GetTempFilePath(name, "txt");
            Logger.Message("Saving text to '{0}'", fileName);
            File.WriteAllText(fileName, contents, Encoding.UTF8);
            ScheduleFilePublishing(fileName);
        }

        public void Publish(string contents, string name)
        {
            var fileName = GetTempFilePath(name);
            File.WriteAllText(fileName, contents, Encoding.UTF8);
            PublishFile(fileName);
        }

        public void PublishQueue()
        {
            string fileName;
            while ((fileName = Queue.FirstOrDefault()) != null)
            {
                PublishFile(fileName);
                Queue.Remove(fileName);
            }

            Tuple<string, string> statItem;
            while ((statItem = StatQueue.FirstOrDefault()) != null)
            {
                stat(statItem.Item1, statItem.Item2);
                StatQueue.Remove(statItem);
            }
        }

        //~Logger()
        //{
        //    PublishQueue();
        //}

        public void stat(string key, string value)
        {
            new TeamCityServiceMessages().CreateWriter().WriteBuildStatistics(key, value);
        }

        public void ScheduleStat(string key, string value)
        {
            StatQueue.Add(new Tuple<string, string>(key, value));
        }
    }
}
