﻿using Fuel.QA.Selenium.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace Fuel.QA.TRUSTe
{
    public class ValidationPage
    {
        // static string partialUrl = @"privacy.truste.com/privacy-seal";
        static By contentLocator = By.Id("validationContent");
        static By companyTitleLocator = By.Id("validationCompanyTitle");
        static By validDateLocator = By.Id("validationValidDate");

        IWebDriver driver;
        string handle;

        public ValidationPage(IWebDriver driver, string handle)
        {
            this.driver = driver;
            this.handle = handle;

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Message = "This is not the TRUSTe Privacy Seal page";
            wait.Until(ExpectedConditions.ElementIsVisible(contentLocator));
        }

        public bool IsMember()
        {
            return driver.HasVisibleElements(companyTitleLocator, validDateLocator);
        }

        public string GetCompany()
        {
            return driver.FindElement(companyTitleLocator).Text;
        }

        public IWebDriver CloseAndReturn()
        {
            driver.Close();
            return driver.SwitchTo().Window(handle);
        }
    }
}
