﻿using AE.Net.Mail;
using Fuel.QA.Core;
using Fuel.QA.HappyStudio.Emails;
using HtmlAgilityPack;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.WebMail
{
    public class GoogleMailIMAP
    {
        string defaultInboxFolderName = "INBOX";
        public ImapClient ImapClient
        {
            get;
            set;
        }

        public GoogleMailIMAP(string host, string login, string password, Int16 port)
        {
            ImapClient = new ImapClient(host, login, password,
                ImapClient.AuthMethods.Login, port, true);
        }

        public void ArchiveAll()
        {
            ImapClient.SelectMailbox(defaultInboxFolderName);
            foreach(var message in ImapClient.GetMessages(0, ImapClient.GetMessageCount(), false))
            {
                //This method archive message, not move message to trash
                ImapClient.DeleteMessage(message);
            }
        }     
    }
}
