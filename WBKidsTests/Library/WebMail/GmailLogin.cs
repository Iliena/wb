﻿using Fuel.QA.Selenium.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace Fuel.QA.WebMail
{
    /// <summary>
    /// Class not finished
    /// </summary>
    public class GmailLogin : LoadableComponent<GmailLogin>
    {
        private const String LOGIN_URL = "https://accounts.google.com/Login";
        private const String LOGOUT_URL = "https://accounts.google.com/Logout";

        [FindsBy(How = How.Id, Using = "Email")]
        IWebElement EmailInput { get; set; }

        [FindsBy(How = How.Id, Using = "Passwd")]
        IWebElement PasswdInput { get; set; }

        [FindsBy(How = How.Id, Using = "PersistentCookie")]
        IWebElement PersistentCookieCheckbox { get; set; }

        [FindsBy(How = How.Id, Using = "signIn")]
        IWebElement SignInButton { get; set; }

        private IWebDriver driver;
        private string Login;
        private string Password;


        public GmailLogin(IWebDriver driver, string login, string password)
        {
            this.driver = driver;
            this.Login = login;
            this.Password = password;

            PageFactory.InitElements<GmailLogin>(driver);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.Url.IndexOf("accounts.google.com/Login", StringComparison.InvariantCultureIgnoreCase) > -1;
        }

        protected override void ExecuteLoad()
        {
            driver.Navigate().GoToUrl(LOGOUT_URL);
            driver.Manage().Cookies.DeleteCookieNamed("ACCOUNT_CHOOSER");
            driver.Navigate().GoToUrl(LOGIN_URL);
        }

        public GmailInbox LoginAs(string email, string password)
        {
            Console.WriteLine("[{0}] Login = {1}, Password = *****", GetType().Name, email);
            TypeEmail(email);
            UncheckPersistentCookie();
            TypePassword(password);
            return SubmitLogin();
        }

        public GmailLogin TypeEmail(string email)
        {
            EmailInput.SetValue(email);
            return this;
        }

        public GmailLogin TypePassword(string password)
        {
            PasswdInput.SetValue(password);
            return this;
        }

        public GmailLogin UncheckPersistentCookie()
        {
            if (PersistentCookieCheckbox.Selected)
                PersistentCookieCheckbox.Click();
            return this;
        }

        public GmailInbox SubmitLogin()
        {
            SignInButton.Submit();
            driver.WaitForPageLoad();
            return new GmailInbox(driver, this);
        }
    }
}