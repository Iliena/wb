﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.WebMail
{
    public class GoogleMailInbox
    {
        private const String INBOX_URL = "https://mail.google.com/mail/?ui=html&zy=e";
        private static By emailCheckboxLocator = By.XPath("//input[@type='checkbox'][@name='t']");
        private static By archiveLocator = By.XPath("//input[@name='nvp_a_arch']");
        private static By emailBodyLocator = By.CssSelector("div.msg");

        private IWebDriver driver;

        public GoogleMailInbox(IWebDriver driver)
        {
            this.driver = driver;
            // ToInbox();
        }

        public GoogleMailInbox Open()
        {
            driver.Navigate().GoToUrl(INBOX_URL);
            return this;
        }

        public GoogleMailInbox ArchiveAll()
        {
            Open();
            Console.WriteLine("[{0}] Archiving messages...", GetType().Name);
            foreach (IWebElement cbox in driver.FindElements(emailCheckboxLocator))
            {
                if (!cbox.Selected)
                    cbox.Click();
            }
            driver.FindElement(archiveLocator).Click();
            return this;
        }

        public List<GMailMessageInfo> GetEmails()
        {
            var emailElements = driver.FindElements(By.XPath("//form[@name='f']/table[2]/tbody/tr[td[2]]"));
            return emailElements.Select(e => new GMailMessageInfo(e)).ToList();
        }

        public IEnumerable<IWebElement> GetEmailsByPartialSubject(List<string> subjects)
        {
            IEnumerable<GMailMessageInfo> emails = GetEmails().Where(email =>
                subjects.Any(subj => email.Subject.IndexOf(subj, StringComparison.InvariantCultureIgnoreCase) > -1)
            );

            foreach (GMailMessageInfo email in emails)
            {
                driver.Navigate().GoToUrl(email.Url);
                yield return driver.FindElement(emailBodyLocator);
            }
        }

        public IEnumerable<IWebElement> GetEmailsByPartialSubject(params string[] subjects)
        {
            return GetEmailsByPartialSubject(subjects.ToList());
        }
    }
}
