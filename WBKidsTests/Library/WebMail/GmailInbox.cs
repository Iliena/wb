﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.WebMail
{
    /// <summary>
    /// Class not finished
    /// </summary>
    public class GmailInbox : LoadableComponent<GmailInbox>
    {
        private const String INBOX_URL = "https://mail.google.com/mail/?ui=html&zy=e";

        //private static By emailCheckboxLocator = By.XPath("//input[@type='checkbox'][@name='t']");
        //private static By archiveLocator = By.XPath("//input[@name='nvp_a_arch']");
        private static By emailBodyLocator = By.CssSelector("div.msg");

        [FindsBy(How = How.XPath, Using = "//input[@type='checkbox'][@name='t']")]
        IList<IWebElement> Checkboxes { get; set; }

        [FindsBy(How = How.Name, Using = "nvp_a_arch")]
        IWebElement ArchiveButton { get; set; }

        private IWebDriver driver;
        private LoadableComponent<GmailLogin> LoginPage;

        public GmailInbox(IWebDriver driver, LoadableComponent<GmailLogin> parent)
        {
            this.driver = driver;
            this.LoginPage = parent;
            PageFactory.InitElements<GmailInbox>(driver);
        }

        protected override bool EvaluateLoadedStatus()
        {
            return driver.Url.Contains("mail.google.com/mail");
        }

        protected override void ExecuteLoad()
        {
            LoginPage.Load();
            driver.Navigate().GoToUrl(INBOX_URL);
        }

        public GmailInbox ArchiveAll()
        {
            Console.WriteLine("[{0}] Archiving messages...", GetType().Name);

            if (Checkboxes.Count > 0)
            {
                foreach (var cbox in Checkboxes.Where(c => !c.Selected))
                    cbox.Click();
                ArchiveButton.Click();
            }

            return this;
        }

        public List<GMailMessageInfo> GetEmails()
        {
            var emailElements = driver.FindElements(By.XPath("//form[@name='f']/table[2]/tbody/tr[td[2]]"));
            return emailElements.Select(e => new GMailMessageInfo(e)).ToList();
        }

        public IEnumerable<IWebElement> GetEmailsByPartialSubject(List<string> subjects)
        {
            IEnumerable<GMailMessageInfo> emails = GetEmails().Where(email =>
                subjects.Any(subj => email.Subject.IndexOf(subj, StringComparison.InvariantCultureIgnoreCase) > -1)
            );

            foreach (GMailMessageInfo email in emails)
            {
                driver.Navigate().GoToUrl(email.Url);
                yield return driver.FindElement(emailBodyLocator);
            }
        }

        public IEnumerable<IWebElement> GetEmailsByPartialSubject(params string[] subjects)
        {
            return GetEmailsByPartialSubject(subjects.ToList());
        }
    }


}
