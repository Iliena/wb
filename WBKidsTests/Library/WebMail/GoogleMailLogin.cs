﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Extensions;

using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WebMail
{
    public class GoogleMailLogin
    {
        #region ULR strings
        private const String LOGIN_URL = "https://accounts.google.com/Login";
        private const String LOGOUT_URL = "https://accounts.google.com/Logout";
        #endregion

        #region Login Page Locators
        private static By emailLocator = By.Id("Email");
        private static By passwdLocator = By.Id("Passwd");
        //private static string emailElementId = "Email";
        //private static string passwdElementId = "Passwd";
        private static By persistentCookieLocator = By.Id("PersistentCookie");
        private static By signInLocator = By.Id("signIn");
        #endregion

        private IWebDriver driver;

        public GoogleMailLogin(IWebDriver driver)
        {
            this.driver = driver;
            Logout();
        }

        public GoogleMailLogin Logout()
        {
            driver.Navigate().GoToUrl(LOGOUT_URL);
            driver.Manage().Cookies.DeleteCookieNamed("ACCOUNT_CHOOSER");
            return this;
        }

        public GoogleMailInbox LoginAs(string email, string password)
        {
            Console.WriteLine("[GoogleMailLogin] Login = {0}, Password = *****", email);
            TypeEmail(email);
            TryUncheckPersistentCookie();

            if (!driver.HasElement(passwdLocator))
            {
                driver.FindElement(signInLocator).Submit();
                driver.WaitElement(passwdLocator, TimeSpan.FromSeconds(2));
            }
            TypePassword(password);
            return SubmitLogin();
        }

        public GoogleMailLogin TypeEmail(string email)
        {
            // Occasionally, SendKeys() fails in FireFox. Using JS instead.
            //driver.FindElement(emailLocator).SendKeys(email);

            // ((IJavaScriptExecutor)driver).ExecuteScript(
            //    String.Format("document.getElementById('{0}').value = '{1}'", emailElementId, email));

            // TODO: Test shorter code
            driver.FindElement(emailLocator).SetValue(email);

            return this;
        }

        public GoogleMailLogin TypePassword(string password)
        {
            // Occasionally, SendKeys() fails in FireFox. Using JS instead.
            // driver.FindElement(passwdLocator).SendKeys(password);

            // ((IJavaScriptExecutor)driver).ExecuteScript(
            //    String.Format("document.getElementById('{0}').value = '{1}'", passwdElementId, password));

            // TODO: Test shorter code
            driver.FindElement(passwdLocator).SetValue(password);

            return this;
        }

        public GoogleMailLogin TryUncheckPersistentCookie()
        {
            try
            {
                IWebElement chBox = driver.FindElement(persistentCookieLocator);
                if (chBox.Selected) chBox.Click();
            }
            catch { }
            return this;
        }

        public GoogleMailInbox SubmitLogin()
        {
            driver.FindElement(signInLocator).Submit();
            driver.WaitForPageLoad();
            if (GmailCaptcha.IsLoaded(driver))
                throw new AccessViolationException("CAPTCHA appeared on the Gmail page");
            return new GoogleMailInbox(driver);
        }
    }
}