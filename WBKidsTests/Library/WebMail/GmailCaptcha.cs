﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.QA.Selenium.Extensions;

namespace Fuel.QA.WebMail
{
    public class GmailCaptcha
    {
        private IWebDriver driver;

        private static By captchaLocator = By.XPath("//img[contains(@alt, 'reCAPTCHA')]");

        public GmailCaptcha(IWebDriver driver) {
            this.driver = driver;
            if (!driver.HasElement(captchaLocator))
                throw new InvalidOperationException("This page doesn't contain captcha");
        }

        public static bool IsLoaded(IWebDriver driver) {
            return driver.HasElement(captchaLocator);
        }
    }
}
