﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.WebMail
{
    public class GMailMessageInfo
    {
        public string From { get; set; }
        public string Subject { get; set; }
        public Uri Url { get; set; }
        public bool Unread { get; set; }
        public GMailMessageInfo(IWebElement element)
        {
            var messageElements = element.FindElements(By.TagName("td"));

            IWebElement fromElement = messageElements[1];
            IWebElement subjectElement = messageElements[2];

            From = fromElement.Text;
            Subject = subjectElement.Text;
            Unread = fromElement.FindElements(By.TagName("b")).Count > 0;

            string url = subjectElement.FindElement(By.TagName("a")).GetAttribute("href");
            Url = new UriBuilder(url).Uri;
        }
    }
}
