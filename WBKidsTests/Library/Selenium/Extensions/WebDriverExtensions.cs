﻿using Fuel.QA.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Fuel.QA.Selenium.Extensions
{
    public static class WebDriverExtensions
    {
        public static DriverType GetDriverType(this IWebDriver drv)
        {
            IWebDriver driver = drv as IWrapsDriver == null ? drv
                : ((IWrapsDriver)drv).WrappedDriver;

            switch (((RemoteWebDriver)driver).Capabilities.BrowserName)
            {
                case "internet explorer":
                    return DriverType.InternetExplorer;
                case "firefox":
                    return DriverType.Firefox;
                case "chrome":
                    return DriverType.Chrome;
                case "phantomjs":
                    return DriverType.PhantomJS;
                default:
                    throw new Exception("Unknown driver type");
            }
        }

        private static Dictionary<DriverType, string[]> driverProcNames = new Dictionary<DriverType, string[]>
        {
            {DriverType.Chrome, new string[] {"chromedriver"}},
            {DriverType.Firefox, new string[] {"plugin-container", "WerFault"}},
            {DriverType.InternetExplorer, new string[] {"IEDriverServer"}},
            {DriverType.PhantomJS, new string[] {"phantomjs"}}
        };

        /// <summary>
        /// Quits driver and kills related processes
        /// </summary>
        /// <param name="driver">IWebDriver instance</param>
        public static void Stop(this IWebDriver driver)
        {
            if (driver != null)
            {
                driver.Quit();

                string[] names;
                if (driverProcNames.TryGetValue(driver.GetDriverType(), out names))
                {
                    Utils.KillProcessNamed(names);
                }
            }
        }

        public static bool TryTakeScreenshot(this IWebDriver driver, out Bitmap bitmap)
        {
            bitmap = null;
            if (driver == null)
            {
                return false;
            }
            try
            {
                var screenshot = driver.TakeScreenshot();
                using (MemoryStream stream = new MemoryStream(screenshot.AsByteArray))
                {
                    bitmap = new Bitmap(stream);
                    return true;
                }
            }
            catch { }
            return false;
        }

        public static bool TryGetPageSource(this IWebDriver driver, out string source)
        {
            source = null;
            try
            {
                source = driver.PageSource;
                return true;
            }
            catch { }
            return false;
        }

        public static string GetBrowserInfo(this IWebDriver driver)
        {
            var remoteDriver = driver as RemoteWebDriver;
            if (remoteDriver == null)
                return null;

            var caps = remoteDriver.Capabilities;

            return string.Format("{0} {1}", caps.BrowserName, caps.Version);
        }

        /// <summary>
        /// Waits until the web page is loaded
        /// </summary>
        /// <param name="timeout"></param>
        public static void WaitForPageLoad(this IWebDriver driver, TimeSpan timeout)
        {
            string state = string.Empty;
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, timeout);

                //Checks every 500 ms whether predicate returns true if returns exit otherwise keep trying till it returns true
                wait.Until(d =>
                {
                    try
                    {
                        state = ((IJavaScriptExecutor)driver).ExecuteScript(@"return document.readyState").ToString();
                    }
                    catch (InvalidOperationException)
                    {
                        //Ignore
                    }
                    catch (NoSuchWindowException)
                    {
                        //when popup is closed, switch to last windows
                        //_driver.SwitchTo().Window(_driver.WindowHandles.Last());
                        throw; // TODO: test throw
                    }
                    //In IE7 there are chances we may get state as loaded instead of complete
                    return (state.Equals("complete", StringComparison.InvariantCultureIgnoreCase) || state.Equals("loaded", StringComparison.InvariantCultureIgnoreCase));

                });
            }
            catch (TimeoutException)
            {
                //sometimes Page remains in Interactive mode and never becomes Complete, then we can still try to access the controls
                if (!state.Equals("interactive", StringComparison.InvariantCultureIgnoreCase))
                    throw;
            }
            catch (NullReferenceException)
            {
                //sometimes Page remains in Interactive mode and never becomes Complete, then we can still try to access the controls
                if (!state.Equals("interactive", StringComparison.InvariantCultureIgnoreCase))
                    throw;
            }
            catch (WebDriverException)
            {
                if (driver.WindowHandles.Count == 1)
                {
                    driver.SwitchTo().Window(driver.WindowHandles[0]);
                }
                state = ((IJavaScriptExecutor)driver).ExecuteScript(@"return document.readyState").ToString();
                if (!(state.Equals("complete", StringComparison.InvariantCultureIgnoreCase) || state.Equals("loaded", StringComparison.InvariantCultureIgnoreCase)))
                    throw;
            }
        }

        public static void WaitForPageLoad(this IWebDriver driver, double timeoutInSeconds = 100)
        {
            driver.WaitForPageLoad(TimeSpan.FromSeconds(timeoutInSeconds));
        }

        public static void WaitForUrlChange(this IWebDriver driver, TimeSpan timeout)
        {
            var url = driver.Url;
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.Message = "WaitForUrlChange timeout";
            wait.Until(d => d.Url != url);
        }

        public static void WaitForUrlChange(this IWebDriver driver, double timeoutInSeconds = 30)
        {
            driver.WaitForUrlChange(TimeSpan.FromSeconds(timeoutInSeconds));
        }

        public static void WaitForUrl(this IWebDriver driver, string url, double timeoutInSeconds = 3)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
            wait.Message = "Url '" + url + "' timed out";
            wait.Until(d => d.Url.Contains(url));
        }

        public static void ScrollToTop(this IWebDriver driver)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,0);");
        }


        /// <summary>
        /// Waits until the element becomes visible in the DOM and returns it
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="locator"></param>
        /// <returns></returns>
        public static IWebElement WaitElement(this IWebDriver driver, By locator, TimeSpan timeout)
        {
            return new WebDriverWait(driver, timeout).Till(ExpectedConditions.ElementIsVisible(locator));
        }

        public static bool HasElement(this IWebDriver driver, By by)
        {
            return driver.FindElements(by).Count != 0;
        }

        public static bool HasElements(this IWebDriver driver, params By[] locators)
        {
            return locators.All(by => driver.HasElement(by));
        }

        public static bool HasVisibleElement(this IWebDriver driver, IWebElement element)
        {
            if (element != default(IWebElement))
            {
                try
                {
                    return element.Displayed && element.Size.Width > 0 && element.Size.Height > 0;
                }
                catch (StaleElementReferenceException)
                {
                    // consider element is invisible if it's NOT attached to the document 
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the value representing if all elements are attached to the dom and visible.
        /// </summary>
        /// <param name="driver">The WebDriver instance</param>
        /// <param name="locators"></param>
        /// <returns></returns>
        public static bool HasVisibleElements(this IWebDriver driver, params By[] locators)
        {
            return locators.All(By => driver.HasVisibleElement(By));
        }

        public static bool HasVisibleElement(this IWebDriver driver, By by)
        {
            IWebElement element = null;

            try
            {
                element = driver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            return driver.HasVisibleElement(element);
        }

        public static bool TryPrintCookies(this IWebDriver driver, TextWriter writer)
        {
            try
            {
                driver.PrintCookies(writer);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void PrintCookies(this IWebDriver driver, TextWriter writer)
        {
            foreach (var c in driver.Manage().Cookies.AllCookies)
            {
                writer.WriteLine(c.ToString());
            }
        }

        public static void PrintCookies(this IWebDriver driver)
        {
            driver.PrintCookies(Console.Out);
        }

        public static string GetStringifiedCookies(this IWebDriver driver)
        {
            return string.Join("\n", driver.Manage().Cookies.AllCookies.Select(c => c.ToString()));
        }

        public static void ClearLocalStorage(this IWebDriver driver)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.localStorage.clear();");   
        }
    }
}
