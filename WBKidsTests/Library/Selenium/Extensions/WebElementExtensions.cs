﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Fuel.QA.Selenium.Extensions
{
    public static class WebElementExtensions
    {
        public static IWebDriver GetDriver(this IWebElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("Element can't be null");
            }

            element = element.GetWrappedElement();

            // Unwrap driver
            IWrapsDriver wrapperElement = element as IWrapsDriver;
            if (wrapperElement == null)
            {
                throw new ArgumentException("Element must wrap a web driver", "element");
            }
            return wrapperElement.WrappedDriver;
        }

        public static IWebElement GetWrappedElement(this IWebElement element)
        {
            for (; ; )
            {
                IWrapsElement wrapped = element as IWrapsElement;
                if (wrapped == null) return element;
                element = wrapped.WrappedElement;
            }
        }

        /// <summary>
        /// Click the mouse on the element with given offset
        /// </summary>
        /// <param name="element">IWebElement instance</param>
        /// <param name="target">Target click point</param>
        public static void Click(this IWebElement element, TargetPosition target = TargetPosition.TopLeft)
        {
            IWebDriver driver = element.GetDriver();

            int offsetX = 0, offsetY = 0;

            switch (target)
            {
                case TargetPosition.Center:
                    offsetX = element.Size.Width / 2;
                    offsetY = element.Size.Height / 2;
                    break;
                case TargetPosition.TopLeft:
                    offsetX = offsetY = 0;
                    break;
                case TargetPosition.TopLeft1px:
                    offsetX = offsetY = 1;
                    break;
                case TargetPosition.BottomLeft10px:
                    offsetX = 10;
                    offsetY = element.Size.Height - 10;
                    break;
            }

            new Actions(driver).MoveToElement(element, offsetX, offsetY).Click().Perform();
        }

        /// <summary>
        /// Clicks on IWebElement by executing JavaScript code
        /// </summary>
        /// <param name="element">IWebElement instance</param>
        public static void ClickJS(this IWebElement element)
        {
            var Driver = element.GetDriver();
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click();", element);
        }

        /// <summary>
        /// Gets the bitmap of given IWebElement
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Element's image</returns>
        public static Bitmap Snapshot(this IWebElement element)
        {
            element.ScrollIntoView();
            Size size = element.Size;
            Point location = element.Location;

            // TakeScreenshot method takes some time
            Screenshot screenshot = element.GetDriver().TakeScreenshot();

            using (MemoryStream stream = new MemoryStream(screenshot.AsByteArray))
            using (Bitmap bitmap = new Bitmap(stream))
            {
                int width = Math.Min(size.Width, bitmap.Width);
                int height = Math.Min(size.Height, bitmap.Height);
                Rectangle crop = new Rectangle(location.X, location.Y, width, height);
                return bitmap.Clone(crop, bitmap.PixelFormat);
            }
        }

        /// <summary>
        /// Sets element's value via JavaScript
        /// </summary>
        /// <param name="element">An instance of IWebElement</param>
        /// <param name="value">Value to be set</param>
        /// <returns>Given instance of IWebElement</returns>
        public static IWebElement SetValue(this IWebElement element, string value)
        {
            var driver = element.GetDriver();
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value=arguments[1]", element, value);
            return element;
        }

        /// <summary>
        /// Makes element visible by changing its CSS properties: visibility=visible, opacity=1
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Returns the given instance of an IWebElement</returns>
        public static IWebElement Show(this IWebElement element)
        {
            var script = "arguments[0].style.visibility='visible';arguments[0].style.opacity=1;return ''";
            element.GetDriver().ExecuteJavaScript<string>(script, element);
            return element;
        }

        /// <summary>
        /// Hides the element by setting CSS properties: visibility=hidden, display=none
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Returns the given instance of an IWebElement</returns>
        public static IWebElement Hide(this IWebElement element)
        {
            var script = "arguments[0].style.visibility='hidden';arguments[0].style.display='none';return ''";
            element.GetDriver().ExecuteJavaScript<string>(script, element);
            return element;
        }

        public static Point GetLocationInViewport(this IWebElement element)
        {
            var driver = element.GetDriver();

            string script = "var c=arguments[0].getBoundingClientRect(); return [c.left, c.top];";
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var coordinates = ((IReadOnlyCollection<Object>)js.ExecuteScript(script, element))
                .Select(c => Convert.ToInt32(c)).ToArray();
            return new Point(coordinates[0], coordinates[1]);
        }

        public static void ScrollToElement(this IWebElement element, int x = 0, int y = 0)
        {
            var driver = element.GetDriver();
            x += element.Location.X;
            y += element.Location.Y;
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("scroll(" + x + ", " + y + ");");
        }

        /// <summary>
        /// Scrolls the element into view.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static IWebElement ScrollIntoView(this IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)element.GetDriver();
            js.ExecuteScript("arguments[0].scrollIntoView(true);", element.GetWrappedElement());
            return element;
        }

        /// <summary>
        /// Tests whether the element is shown on the screen
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Value indicating element's visibility</returns>
        public static bool IsVisibleInViewport(this IWebElement element)
        {
            var size = element.GetDriver().Manage().Window.Size;
            var location = element.GetLocationInViewport();
            return size.Width > location.X && size.Height > location.Y;
        }

        /// <summary>
        /// Tests whether the attribute changes during interval 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="attribute">Element's attribute to inspect</param>
        /// <param name="interval">The maximum time to wait for a change</param>
        /// <returns>Value indicating if attribute changed during given time interval</returns>
        public static bool isAttributeChanging(this IWebElement element, string attribute, TimeSpan interval)
        {
            String value = element.GetAttribute(attribute);
            DefaultWait<IWebElement> wait = new DefaultWait<IWebElement>(element);
            wait.Timeout = interval;
            return wait.Till(e => !e.GetAttribute(attribute).Equals(value));
        }

        /// <summary>
        /// Returns the value representing if element is changing its location
        /// </summary>
        /// <param name="element">IWebElement instance</param>
        /// <param name="interval">The interval to poll element's location</param>
        /// <returns></returns>
        public static bool IsImmovable(this IWebElement element, TimeSpan interval)
        {
            Point location = element.Location;
            DefaultWait<IWebElement> wait = new DefaultWait<IWebElement>(element);
            wait.PollingInterval = TimeSpan.FromMilliseconds(50);
            wait.Timeout = interval;

            return !wait.Till(e => e.Location != location);
        }

        /// <summary>
        /// Returns the value representing if element is changing its location
        /// </summary>
        /// <param name="element">IWebElement instance</param>
        /// <param name="intervalInMilliseconds">The interval to poll element's location</param>
        /// <returns></returns>
        public static bool IsImmovable(this IWebElement element, double intervalInMilliseconds = 250)
        {
            return element.IsImmovable(TimeSpan.FromMilliseconds(intervalInMilliseconds));
        }


        /// <summary>
        /// Tests whether the element is Displayed and it's size is not zero. Returns false for null values.
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Value indicating whether the element is displayed</returns>
        public static bool IsDisplayed(this IWebElement element)
        {
            if (element != null)
            {
                try
                {
                    Size size = element.Size;
                    return size.Height > 0 && size.Width > 0 && element.Displayed;
                }
                catch (StaleElementReferenceException) { }
            }
            return false;
        }



        public static bool IsAttached(this IWebElement element)
        {
            try
            {
                return !element.Size.Equals(Point.Empty);
            }
            catch (Exception e)
            {
                if (e is StaleElementReferenceException || e.GetBaseException() is StaleElementReferenceException)
                    return false;
                if (e is NoSuchElementException || e.GetBaseException() is NoSuchElementException)
                    return false;

                throw;
            }
        }

        /// <summary>
        /// Tests whether the image represented by an IWebElement instance is loaded
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool IsImageLoaded(this IWebElement element)
        {
            var driver = element.GetDriver();
            var script = "var e=arguments[0];return e.complete && typeof e.naturalWidth != 'undefined' && e.naturalWidth > 0";
            var status = (bool)((IJavaScriptExecutor)driver).ExecuteScript(script, element);
            return status == true;
        }

        /// <summary>
        /// Highlights element on the page
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static void HighlightElement(this IWebElement element)
        {
            var driver = element.GetDriver();
            var elem = element.GetWrappedElement();
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].style.cssText ='border-width: 3px; border-style: solid; border-color: red; background-color:  rgba(255, 255, 0, 0.5);'", elem);
        }

        public static bool TryHighlight(this IWebElement element)
        {
            try
            {
                element.HighlightElement();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        /*
        public static bool IsImageLoaded(this IWebElement element)
        {
            IWebDriver driver = element.GetDriver();
            return (Boolean)((IJavaScriptExecutor)driver)
                .ExecuteScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", element);
        }
        */

        static string SafelyGetHTMLInfo(IWebElement element, Func<IWebElement, string> getter)
        {
            try
            {
                return getter(element);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        static string FormatAttribute(string name, string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : string.Format(" {0}='{1}'", name, value);
        }

        public static string HTMLDummy(this IWebElement element)
        {
            string tag = element.TagName;
            string text = element.Text;
            string cls = FormatAttribute("class", SafelyGetHTMLInfo(element, e => e.GetAttribute("class")));
            string id = FormatAttribute("id", SafelyGetHTMLInfo(element, e => e.GetAttribute("id")));
            string title = FormatAttribute("title", SafelyGetHTMLInfo(element, e => e.GetAttribute("title")));
            string attString = id + title + cls;
            return string.Format("<{0}{2}>{1}</{0}>", tag, text, attString);
        }
    }

    public enum TargetPosition
    {
        Center, TopLeft, TopLeft1px, BottomLeft10px
    }
}
