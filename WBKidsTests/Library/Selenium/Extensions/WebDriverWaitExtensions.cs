﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Selenium.Extensions
{
    public static class WebDriverWaitExtensions
    {
        /// <summary>
        /// Repeatedly applies this instance's input value to the given function until
        /// one of the following occurs:
        /// - the function returns neither null nor false;
        /// - the function throws an exception that is not in the list of ignored exception types;
        /// - the timeout expires.
        /// </summary>
        /// <typeparam name="TResult">The delegate's expected return type.</typeparam>
        /// <param name="wait">An instance of WebDriverWait</param>
        /// <param name="condition">A delegate taking an object of type T as its parameter, and returning a TResult.</param>
        /// <returns>The delegate's return value.</returns>
        public static TResult Till<TResult, TSource>(this IWait<TSource> wait, Func<TSource, TResult> condition)
        {
            try
            {
                return wait.Until(condition);
            }
            catch (WebDriverTimeoutException) { }
            catch (StaleElementReferenceException) { }
            return default(TResult);
        }
    }
}
