﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Selenium.Extensions
{
    public static class ICookieJarExtensions
    {
        public static CookieContainer GetAllCookies(this ICookieJar cookieJar)
        {
            CookieContainer container = new CookieContainer();

            foreach (var cookie in cookieJar.AllCookies)
            {
                var netCookie = new System.Net.Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain);

                // Cookie.Domain is null in IEDriver. See ticket: 
                // https://code.google.com/p/selenium/issues/detail?id=4445
                // Workaround: use driver's Host as cookie domain
                if (string.IsNullOrEmpty(netCookie.Domain))
                {
                    var fieldInfo = cookieJar.GetType().GetField("driver", BindingFlags.Instance | BindingFlags.NonPublic);
                    IWebDriver driver = (IWebDriver)fieldInfo.GetValue(cookieJar);
                    netCookie.Domain = new Uri(driver.Url).Host;
                }
                container.Add(netCookie);
            }
            return container;
        }
    }
}
