﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Internal;

using System.Drawing;

using Fuel.QA.WinAPI;
using Fuel.QA.Core;

namespace Fuel.QA.Selenium.Extensions
{
    /// <summary>
    /// Supplies a set of common conditions that can be waited for using OpenQA.Selenium.Support.UI.WebDriverWait.
    /// This is an extension to OpenQA.Selenium.Support.UI.ExpectedConditions
    /// </summary>
    public class ExpectedConditionsExtension
    {
        /// <summary>
        /// An expectation for checking that an element is not present on the DOM of a page
        /// or invisible. Invisibility means that the element is not displayed or has a height 
        /// or width equals to 0.
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        public static Func<IWebDriver, bool> ElementIsInvisible(By locator)
        {
            return ElementsAreInvisible(locator);
        }

        /// <summary>
        /// An expectation for checking that ALL elements are invisible.
        /// </summary>
        /// <param name="locators"></param>
        /// <returns></returns>
        public static Func<IWebDriver, bool> ElementsAreInvisible(params By[] locators)
        {
            return driver =>
            {
                return locators.ToList().All(locator =>
                {
                    try
                    {
                        var element = driver.FindElement(locator);
                        return element == null || !element.Displayed || element.Size.Height == 0 || element.Size.Width == 0;
                    }
                    catch (StaleElementReferenceException)
                    {
                        return true; // Element is not attached to the DOM meaning it's invisible
                    }
                    catch (NoSuchElementException)
                    {
                        return true; // Element is not found meaning it's invisible
                    }
                    catch (InvalidOperationException)
                    {
                        // Ignore
                        return false;
                    }
                });
            };
        }

        //public static Func<IWebDriver, bool> ElementsAreDisplayed(params By[] locators)
        //{
        //    return driver => locators.All(locator => driver.FindElement(locator).Displayed);
        //}

        public static Func<IWebDriver, IWebElement> AnyElementVisible(params By[] locators)
        {
            return driver =>
            {
                foreach (var locator in locators)
                {
                    try
                    {
                        IWebElement element = driver.FindElement(locator);
                        if (element.Displayed && element.Size.Height > 0 && element.Size.Width > 0)
                            return element;
                    }
                    catch (NoSuchElementException)
                    {
                        // continue
                    }
                    catch (StaleElementReferenceException)
                    {
                        // continue
                    }
                    catch (InvalidOperationException)
                    {
                        // ignore
                    }
                }
                return null;
            };
        }

        /// <summary>
        /// An expectation for checking that FindElement function doesn't rise the
        /// InvalidOperationException for given locators. This is a workaround for
        /// FireFox/New Relic bug.
        /// See https://code.google.com/p/selenium/issues/detail?id=7170
        /// </summary>
        /// <param name="locators"></param>
        /// <returns></returns>
        public static Func<IWebDriver, bool> ElementsAreAccessible(params By[] locators)
        {
            return driver =>
            {
                foreach (By locator in locators)
                {
                    try
                    {
                        driver.FindElement(locator);
                    }
                    catch (NoSuchElementException) { } // ignore
                    catch (InvalidOperationException) { return false; }
                }
                return true;
            };
        }

        //public static Func<IWebDriver, bool> ElementsValueIs(IWebElement element, string value)
        //{
        //    return driver => element.GetAttribute("value") == value;
        //}

        //public static Func<IWebDriver, bool> ElementIsImmovable(IWebElement element, TimeSpan interval)
        //{
        //    return driver =>
        //    {
        //        DateTime started = DateTime.Now;
        //        Point location = element.Location;
        //        WebDriverWait wait = new WebDriverWait(element.GetDriver(), interval);
        //        wait.PollingInterval = TimeSpan.FromMilliseconds(50);
        //        return !wait.Till(d => element.Location != location || DateTime.Now - started < interval);
        //    };
        //}

        //public static Func<IWebDriver, bool> ElementIsLookingLike(IWebElement element, Bitmap bitmap, int tolerance)
        //{
        //    return driver =>
        //    {
        //        return element.isLookingLike(bitmap, tolerance);
        //    };
        //}

        //public static Func<IWebDriver, bool> ElementMatchesPattern(IWebElement element, IEnumerable<Pattern> patterns)
        //{
        //    return driver =>
        //    {
        //        return patterns.Any(p => element.isLookingLike(p.Bitmap, p.Tolerance));
        //    };
        //}
    }
}
