﻿using Fuel.QA.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;

namespace Fuel.QA.Selenium
{
    public enum DriverType { InternetExplorer, Chrome, Firefox, PhantomJS };

    public class WebDriverUtils
    {
        private static IWebDriver InitDriver(DriverType type, Size windowSize, bool maximized)
        {
            Console.WriteLine("Initializing '{0}' driver...", type);
            IWebDriver driver = null;
            switch (type)
            {
                case DriverType.Chrome:
                    ChromeOptions chOptions = new ChromeOptions();

                    // To remove message "You are using an unsupported command-line flag: --ignore-certificate-errors
                    // Stability and security will suffer."
                    chOptions.AddArgument("test-type");

                    // Enables the use of NPAPI plugins.
                    chOptions.AddArgument("enable-npapi");
                    // Disables the bundled PPAPI version of Flash.
                    chOptions.AddArgument("disable-bundled-ppapi-flash");
                    // Prevents Chrome from requiring authorization to run certain widely installed but less commonly used plugins.
                    chOptions.AddArgument("always-authorize-plugins");
                    // Disables the Web Notification and the Push APIs.
                    chOptions.AddArgument("disable-notifications");

                    // More Chrome command line switches:
                    // http://peter.sh/experiments/chromium-command-line-switches/

                    if (!windowSize.IsEmpty)
                        chOptions.AddArgument(string.Format("window-size={0},{1}", windowSize.Width, windowSize.Height));

                    driver = new ChromeDriver(chOptions);
                    break;
                case DriverType.InternetExplorer:
                    var ieOptions = new InternetExplorerOptions()
                    {
                        IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                        EnsureCleanSession = true,
                        InitialBrowserUrl = "about:blank"
                    };
                    driver = new InternetExplorerDriver(ieOptions);
                    break;
                case DriverType.Firefox:
                    driver = new FirefoxDriver();
                    break;
                case DriverType.PhantomJS:
                    driver = new PhantomJSDriver();
                    break;
            }
            if (!windowSize.IsEmpty)
                driver.Manage().Window.Size = windowSize;
            if (maximized)
                driver.Manage().Window.Maximize();
            return driver;
        }
        
        public static IWebDriver InitDriver(DriverType type, bool maximized = false)
        {
            return InitDriver(type, Size.Empty, maximized);
        }

        public static IWebDriver InitDriver(DriverType type, Size windowSize)
        {
            return InitDriver(type, windowSize, false);
        }

        public static IWebDriver InitLoagableDriver(DriverType type, Size windowSize)
        {
            var driver = InitDriver(type, windowSize);
            return new LoggableWebDriver(driver);
        }

        public static IWebDriver InitLoagableDriver(DriverType type)
        {
            return InitLoagableDriver(type, Size.Empty);
        }

        public static void KillAll()
        {
            string[] processNames = new string[] {
                "chromedriver", "IEDriverServer", "iexplore", "phantomjs", "plugin-container", "WerFault"
            };

            var processes = new List<Process>();
            foreach (var proc in processNames)
            {
                try
                {
                    processes.AddRange(Process.GetProcessesByName(proc));
                }
                catch (InvalidOperationException) { }
            }

            foreach (var proc in processes)
            {
                try
                {
                    proc.Kill();
                    Console.WriteLine("Killed process: [{0}] {1}", proc.Id, proc.ProcessName);
                }
                catch (Exception) { }
            }
        }
    }
}
