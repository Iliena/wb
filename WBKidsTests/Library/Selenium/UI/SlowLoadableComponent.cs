﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace Fuel.QA.Selenium.UI
{
    public abstract class SlowLoadableComponentEx<T>
        : SlowLoadableComponent<T> where T : SlowLoadableComponentEx<T>
    {
        public SlowLoadableComponentEx(TimeSpan timeout)
            : base(timeout) { }

        public override T Load()
        {
            try
            {
                return base.Load();
            }
            catch (WebDriverTimeoutException e)
            {
                if (e.Message.IndexOf(UnableToLoadMessage) < 0)
                {
                    throw new WebDriverTimeoutException(UnableToLoadMessage, e);
                }
                throw;
            }
        }

        public new bool IsLoaded { get { return base.IsLoaded; } }
    }
}
