﻿using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuel.QA.Selenium.UI
{
    public abstract class LoadableComponentEx<T> : LoadableComponent<T>
        where T : LoadableComponent<T>
    {
        public new bool IsLoaded { get { return base.IsLoaded; } }
    }
}
