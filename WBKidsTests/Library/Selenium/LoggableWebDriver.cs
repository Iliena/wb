﻿using Fuel.QA.Selenium.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Internal;
using System;

namespace Fuel.QA.Selenium
{
    /// <summary>
    /// A wrapper around an arbitrary WebDriver instance which logs events to Console
    /// </summary>
    public class LoggableWebDriver : EventFiringWebDriver
    {
        public LoggableWebDriver(IWebDriver driver)
            : base(driver)
        {
            #region WebDriver events
            Navigating += new EventHandler<WebDriverNavigationEventArgs>((sender, e) =>
            {
                Console.WriteLine("Navigating to: " + e.Url);
            });

            ScriptExecuting += new EventHandler<WebDriverScriptEventArgs>((sender, e) =>
            {
                Console.WriteLine("Executing script: " + e.Script);
            });
            #endregion

            #region WebElement events
            ElementValueChanging += new EventHandler<WebElementEventArgs>((sender, e) =>
            {
                Console.WriteLine("ElementValueChanging: " + e.Element.HTMLDummy());
            });

            ElementClicking += new EventHandler<WebElementEventArgs>((sender, e) =>
            {
                Console.WriteLine("ElementClicking: " + e.Element.HTMLDummy());
            });

            FindingElement += new EventHandler<FindElementEventArgs>((sender, e) =>
            {
                Console.WriteLine("FindingElement: " + e.FindMethod);
            });
            #endregion
        }
    }
}
